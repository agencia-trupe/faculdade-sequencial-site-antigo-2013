$('document').ready( function(){

	$('#slideshow #animate').cycle({
        timeout: 6000,
        fx : 'fade',
        pager : '#bullets',
        activePagerClass: 'activePager',
        pagerAnchorBuilder: function(idx, slide) { 
            return "<a href=''></div>";
        }
    });
    
    $('.fancy').fancybox();

	$('.link-tipos-cursos').click( function(e){

		e.preventDefault();

		var destino = $(this).attr('href').split('/').pop();
		var titulos = {
			graduacao : 'graduação',
			pos_graduacao : 'pós-graduação',
			extensao_universitaria : 'extensão universitária',
			cursos_livres : 'cursos livres'
		}
		var titulo_pagina = $('section h1:first');
		var titulo_pagina_texto = $('section h1:first').html();

		$('.link-tipos-cursos.ativo').removeClass('ativo');
		$(this).addClass('ativo');
		$(this).parent('.container').addClass('ativo');

		if($('.container.parent.'+destino).is(':visible')){
			$('.container.parent:not(.'+destino+')').fadeOut();
		}else{
			$('.container.parent:visible').fadeOut('normal', function(){
				$('.container.parent.'+destino).fadeIn();	
			});
		}

		if($('#breadcrumbs a').length > 2){

			$('#breadcrumbs a:last').fadeOut('normal', function(){
				$('#breadcrumbs a:last').remove();
				$('#breadcrumbs').append($("<a href='cursos/index/"+destino+"' title='"+titulos[destino]+"'>"+titulos[destino]+"</a>")).fadeIn();
			});
		}else{
			$('#breadcrumbs').append("&raquo;&nbsp;").append($("<a href='cursos/index/"+destino+"' title='"+titulos[destino]+"'>"+titulos[destino]+"</a>")).fadeIn();
		}

		if(titulo_pagina_texto.indexOf('•') != -1){
			titulo_pagina.html(titulo_pagina_texto.substring(0, titulo_pagina_texto.indexOf('•')) + " • " + titulos[destino].charAt(0).toUpperCase() + titulos[destino].substring(1));
		}else{
			titulo_pagina.append("• " + titulos[destino].charAt(0).toUpperCase() + titulos[destino].substring(1));
		}

		window.location.hash = destino;
		
	});

});