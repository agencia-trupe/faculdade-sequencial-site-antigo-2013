$(document).ready( function(){

    $('.voltar').click( function(){ window.history.back(); });

    $('#datepicker').datepicker();

    if($('.mostrarerro').length || $('.mostrarsucesso').length){
        $('.mensagem').show('normal', function(){
            $(this).delay(4000).slideUp('normal');
        })
    }

    $('.delete').click( function(){
        if(!confirm('Deseja Excluir o Registro ?'))
            return false;
    });

    $('a.move').live('click', function(e){
        e.preventDefault();
    })

    tinyMCE.init({
        language : "pt",
        mode : "specific_textareas",
        editor_selector : "topico",
        theme : "advanced",
        theme_advanced_buttons1 : "tablecontrols,separator,code,separator,bold,italic,underline,bullist,separator,link,unlink,separator,jbimages",
        theme_advanced_buttons2 : "",
        theme_advanced_buttons3 : "",
        theme_advanced_toolbar_location : "top",
        plugins: "table,paste,jbimages",
        paste_text_sticky : true,
        setup : function(ed) {
            ed.onInit.add(function(ed) {
                ed.pasteAsPlainText = true;
            });
        },
        width:600,
        height:200,
        valid_children : "+body[style]",
        valid_elements : '*[*]'
    });

    tinyMCE.init({
        language : "pt",
        mode : "specific_textareas",
        editor_selector : "comimagem",
        theme : "advanced",
        theme_advanced_buttons1 : "tablecontrols,separator,code,separator,bold,italic,underline,separator,link,unlink,separator,jbimages",
        theme_advanced_buttons2 : "",
        theme_advanced_buttons3 : "",
        theme_advanced_toolbar_location : "top",
        plugins: "table,paste,jbimages",
        paste_text_sticky : true,
        setup : function(ed) {
            ed.onInit.add(function(ed) {
                ed.pasteAsPlainText = true;
            });
        },
        width:800,
        height:400,
        valid_children : "+body[style]",
        valid_elements : '*[*]'
    });

    tinyMCE.init({
        language : "pt",
        mode : "specific_textareas",
        editor_selector : "semimagem",
        theme : "advanced",
        theme_advanced_buttons1 : "formatselect,separator,bold,italic,underline,separator,link,unlink",
        theme_advanced_buttons2 : "",
        theme_advanced_buttons3 : "",
        theme_advanced_blockformats : "h1, h2, p",
        theme_advanced_toolbar_location : "top",
        plugins: "paste",
        paste_text_sticky : true,
        setup : function(ed) {
            ed.onInit.add(function(ed) {
                ed.pasteAsPlainText = true;
            });
        },
        valid_children : "+body[style]",
        valid_elements : '*[*]'
    });

    tinyMCE.init({
        language : "pt",
        mode : "specific_textareas",
        editor_selector : "basico",
        theme : "advanced",
        theme_advanced_buttons1 : "bold,italic,underline,link,unlink",
        theme_advanced_buttons2 : "",
        theme_advanced_buttons3 : "",
        theme_advanced_toolbar_location : "top",
        plugins: "paste",
        paste_text_sticky : true,
        setup : function(ed) {
            ed.onInit.add(function(ed) {
                ed.pasteAsPlainText = true;
            });
        },
        width:660,
        height:150,
        valid_children : "+body[style]",
        valid_elements : '*[*]'
    });
});