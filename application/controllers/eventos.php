<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Eventos extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();

      $this->load->model('eventos_model', 'model');
    }

    function index($pag = 0){

      $this->load->library('pagination');
      $config['base_url'] = base_url('eventos/index/');
      $config['per_page'] = 10;
      $config['uri_segment'] = 3;
      $config['next_link'] = FALSE;
      $config['prev_link'] = FALSE;
      $config['num_links'] = 10;
      $config['first_link'] = FALSE;
      $config['last_link'] = FALSE;
      $config['total_rows'] = $this->db->where('data <', date('Y-m-d'))->get('eventos')->num_rows();
      $this->pagination->initialize($config);
      $data['paginacao'] = $this->pagination->create_links();
      $data['eventos'] = $this->model->pegarPaginado($config['per_page'], $pag, 'data', 'desc');

      $data['proximos_eventos'] = $this->db->where('data >=', date('Y-m-d'))->get('eventos')->result();

   		$this->load->view('eventos/index', $data);
   	}

    function detalhes($slug = false){

      if (!$slug)
        redirect('eventos/index');

      $data['detalhe'] = $this->db->get_where('eventos', array('slug' => $slug))->result();

      if(!isset($data['detalhe'][0]))
        redirect('eventos/index');

      $data['galeria'] = $this->db->get_where('eventos_imagens', array('id_parent' => $data['detalhe'][0]->id))->result();
      $this->load->view('eventos/detalhes', $data);
    }

}