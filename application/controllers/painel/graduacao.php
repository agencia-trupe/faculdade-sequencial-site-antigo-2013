<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Graduacao extends MY_Admincontroller {

    function __construct(){
   		parent::__construct();

   		$this->load->model('graduacao_model', 'model');
    }

    function form($id = false){
        if($id){
            $data['registro'] = $this->model->pegarPorId($id);
            if(!$data['registro'])
                redirect('painel/'.$this->router->class);

            $data['topicos'] = $this->db->order_by('ordem', 'asc')->get_where('graduacao_topicos', array('id_parent' => $id))->result();

        }else{
            $data['registro'] = FALSE;
        }

        $data['titulo'] = $this->titulo;
        $data['unidade'] = $this->unidade;
        $this->load->view('painel/'.$this->router->class.'/form', $data);
    }    

}