<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Noticias extends MY_Admincontroller {

    function __construct(){
   		parent::__construct();

   		$this->load->model('noticias_model', 'model');
    }

    function index(){
        $data['registros'] = $this->model->pegarTodos('data', 'desc');

        $data['titulo'] = $this->titulo;
        $data['unidade'] = $this->unidade;
        $data['campo_1'] = $this->campo_1;
        $data['campo_2'] = $this->campo_2;
        $data['campo_3'] = $this->campo_3;
        $this->load->view('painel/'.$this->router->class.'/lista', $data);
    }

}