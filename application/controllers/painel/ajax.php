<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller {

   function __construct(){
   		parent::__construct();

   		if(!$this->input->is_ajax_request())
   			redirect('home');
   }

	function pegaTopicos(){
		$id_textos = $this->input->post('id_textos');
		$tabela = $this->input->post('tabela');

		$qry = $this->db->order_by('ordem', 'asc')->get_where($tabela, array('id_parent' => $id_textos))->result();

		foreach ($qry as $key => $value)
			$value->texto = word_limiter($value->texto, 10);

		echo json_encode($qry);
	}

	function pegarTopico(){
		$id_topico = $this->input->post('id_topico');
		$tabela = $this->input->post('tabela');

		$qry = $this->db->get_where($tabela, array('id' => $id_topico))->result();

		echo json_encode($qry);		
	}
	
	function insereTopico(){
		$tabela = $this->input->post('tabela');

		$this->db->set('titulo', $this->input->post('titulo'))
			     ->set('texto', $this->input->post('texto'))
			     ->set('id_parent', $this->input->post('id_textos'))
			     ->set('cor', $this->input->post('cor'))
			     ->insert($tabela);
	}

	function alteraTopico(){
		$tabela = $this->input->post('tabela');

		$this->db->set('titulo', $this->input->post('titulo'))
			     ->set('texto', $this->input->post('texto'))
			     ->set('cor', $this->input->post('cor'))
			     ->where('id', $this->input->post('id_topico'))
			     ->update($tabela);
	}	

	function excluiTopico(){
		$tabela = $this->input->post('tabela');

		$this->db->delete($tabela, array('id' => $this->input->post('id')));
	}

	function gravaOrdem(){
        $menu = $this->input->post('data');
        for ($i = 0; $i < count($menu); $i++) {
            $this->db->set('ordem', $i)
            		 ->where('id', $menu[$i])
            		 ->update($this->input->post('tabela'));
        }
	}

	function geraTopicosPadrao(){
		$id = $this->input->post('id_textos');
		$tabela = $this->input->post('tabela');

		if($tabela == 'graduacao_topicos'):

			$topicos = array(
				array( 'Apresentação do Curso', 'azul'),
				array( 'Legislação', 'azul'),
				array( 'Propostas', 'azul'),
				array( 'Objetivos', 'azul'),
				array( 'Público alvo', 'azul'),
				array( 'Certificação', 'azul'),
				array( 'Diferencial', 'azul'),
				array( 'Duração do curso', 'azul'),
				array( 'Horários das aulas', 'azul'),
				array( 'Modalidade do curso', 'azul'),
				array( 'Área de atuação profissional', 'azul'),
				array( 'Carga horária total', 'azul'),
				array( 'Valor do investimento', 'azul'),
				array( 'Documentos Necessários', 'azul'),
				array( 'Formação de turma', 'azul'),
				array( 'Calendário Acadêmico', 'laranja'),
				array( 'Matriz Curricular', 'laranja'),
				array( 'Manual do aluno', 'laranja'),
				array( 'Corpo docente e Coordenação do Curso', 'laranja'),
				array( 'Manual de estágio', 'laranja'),
				array( 'Manual de atividades complementares', 'laranja'),
			);

		else:

			$topicos = array(
				array( 'Apresentação do Curso', 'azul'),
				array( 'Área', 'azul'),
				array( 'Legislação', 'azul'),
				array( 'Propostas', 'azul'),
				array( 'Objetivos', 'azul'),
				array( 'Público alvo', 'azul'),
				array( 'Certificação', 'azul'),
				array( 'Diferencial', 'azul'),
				array( 'Duração do curso', 'azul'),
				array( 'Horários das aulas', 'azul'),
				array( 'Modalidade do curso', 'azul'),
				array( 'Área de atuação profissional', 'azul'),
				array( 'Carga horária total', 'azul'),
				array( 'Valor do investimento', 'azul'),
				array( 'Documentos Necessários', 'azul'),
				array( 'Formação de turma', 'azul'),
				array( 'Calendário Acadêmico', 'laranja'),
				array( 'Matriz Curricular', 'laranja'),
				array( 'Manual do aluno', 'laranja'),
				array( 'Corpo docente e Coordenação do Curso', 'laranja'),
				array( 'Manual de estágio', 'laranja'),
				array( 'Manual de atividades complementares', 'laranja'),
				array( 'Processo seletivo', 'laranja')
			);	
					
		endif;

		foreach ($topicos as $key => $value) {
			$insert =$this->db->set('titulo', $value[0])
							  ->set('texto', 'Alterar...')
							  ->set('id_parent', $id)
							  ->set('ordem', $key)
							  ->set('cor', $value[1])
							  ->insert($tabela);
		}
	}

}