<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();
    }

    function index(){

    	$data['slides'] = $this->db->get('slides')->result();
    	$data['mural'] = $this->db->order_by('ordem', 'ASC')->get('mural')->result();
    	$data['novidades'] = $this->db->get('novidades')->result();
    	$data['chamadas'] = $this->db->get('chamadas', 2, 0)->result();
    	$data['depoimentos'] = $this->db->get('depoimentos', 2, 0)->result();

   		$this->load->view('home', $data);
   	}

}