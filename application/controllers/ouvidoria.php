<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ouvidoria extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();
    }

    function index(){
      $this->load->view('ouvidoria'); 
   	}

   	function enviar(){

   		$this->session->set_flashdata('enviado', TRUE);

   		redirect('ouvidoria/index', 'refresh');
   	}

}