<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property CI_Loader $load
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Email $email
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 */
class Download extends CI_controller {

    function __construct() {
        parent::__construct();
    }


    function index($dir = FALSE, $filename = FALSE, $extensao = '.pdf') {

        if (!$dir || !$filename){
            redirect('erro/index');
        }

        $filepath = $dir.'/';

        $filename = $filename . $extensao;

        $full = $filepath.$filename;
        
        if(!$filename || !file_exists($filepath.$filename)){
            redirect('erro/index');
        }else{
        
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header("Content-Type: application/force-download");
            header('Content-Disposition: attachment; filename=' . urlencode(basename($filename)));
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($full));
            ob_clean();
            flush();
            readfile($full);
            exit;
        }
    }


}
?>
