<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Erro extends CI_controller {

	var $headervar,
		$menuvar,
		$asidevar,
		$footervar;

    function __construct(){
   		parent::__construct();

   		$this->headervar['load_css'] = array();
   		$this->headervar['load_js'] = array();

   		$this->asidevar['destaques'] = $this->db->get('destaques')->result();
    }

    function index(){
    	$output = "<section><h1 style='padding:40px 0; text-align:center;'>Página não encontrada.</h1></section>";
        
        echo $this->load->view('common/header', $this->headervar, TRUE).
             $this->load->view('common/menu', $this->menuvar, TRUE).
             $output.
             $this->load->view('common/aside-destaques', $this->asidevar, TRUE).
             $this->load->view('common/footer', $this->footervar, TRUE);
   		
   	}

   	function emBreve(){
   		$output = "<section><h1 style='padding:40px 0; text-align:center;'>Em Breve.</h1></section>";
        
        echo $this->load->view('common/header', $this->headervar, TRUE).
             $this->load->view('common/menu', $this->menuvar, TRUE).
             $output.
             $this->load->view('common/aside-destaques', $this->asidevar, TRUE).
             $this->load->view('common/footer', $this->footervar, TRUE);
   	}

}