<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cursos extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();
    }

    function index($mostrar = false){

      $data['pos'] = $this->db->get('pos_graduacao')->result();
      $data['extensao'] = $this->db->order_by('data_inicio', 'DESC')->get('cursos_extensao')->result();
      $data['livres'] = $this->db->order_by('data_inicio', 'DESC')->get('cursos_livres')->result();

      $data['ativo'] = $mostrar;

   		$this->load->view('cursos/index', $data);
   	}

    function graduacao($slug_curso = false){

      if($slug_curso){

        $query = $this->db->get_where('graduacao', array('slug' => $slug_curso))->result();

        if(!$query[0])
          redirect('cursos/index/graduacao');

        $data['detalhes'] = $query[0];
        $data['topicos'] = $this->db->order_by('ordem', 'asc')->get_where('graduacao_topicos', array('id_parent' => $data['detalhes']->id))->result();
      }else{
        redirect('cursos/index/graduacao');
      }

      $this->breadcrumbs[2] = $slug_curso;
      $this->load->view('cursos/graduacao', $data);
    }

    function pos_graduacao($slug_curso = false){
      if($slug_curso){

        $query = $this->db->get_where('pos_graduacao', array('slug' => $slug_curso))->result();

        if(!$query[0])
          redirect('cursos/index/pos_graduacao');

        $data['detalhes'] = $query[0];
        $data['topicos'] = $this->db->order_by('ordem', 'asc')->get_where('pos_graduacao_topicos', array('id_parent' => $data['detalhes']->id))->result();
      }else{
        redirect('cursos/index/pos_graduacao');
      }

      $this->breadcrumbs[2] = $slug_curso;
      $this->load->view('cursos/pos_graduacao', $data);
    }

    function extensao_universitaria($slug_curso = false){
      if($slug_curso){

        $query = $this->db->order_by('data_inicio', 'DESC')->get_where('cursos_extensao', array('slug' => $slug_curso))->result();

        if(!$query[0])
          redirect('cursos/index/extensao_universitaria');

        $data['detalhes'] = $query[0];
      }else{
        redirect('cursos/index/extensao_universitaria');
      }

      $this->breadcrumbs[2] = $slug_curso;
      $data['cursos'] = $this->db->order_by('data_inicio', 'DESC')->get_where('cursos_extensao', array('id !=' => $data['detalhes']->id))->result();
      $this->load->view('cursos/extensao_universitaria', $data);
    }

    function cursos_livres($slug_curso = false){
      if($slug_curso){

        $query = $this->db->order_by('data_inicio', 'DESC')->get_where('cursos_livres', array('slug' => $slug_curso))->result();

        if(!$query[0])
          redirect('cursos/index/cursos_livres');

        $data['detalhes'] = $query[0];
      }else{
        redirect('cursos/index/cursos_livres');
      }

      $this->breadcrumbs[2] = $slug_curso;
      $data['cursos'] = $this->db->order_by('data_inicio', 'DESC')->get_where('cursos_livres', array('id !=' => $data['detalhes']->id))->result();
      $this->load->view('cursos/cursos_livres', $data);
    }

}