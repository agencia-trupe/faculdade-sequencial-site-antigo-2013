<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contato extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();
    }

    function index(){
      $this->load->view('contato'); 
   	}

   	function enviar(){

   		$this->session->set_flashdata('enviado', TRUE);

   		redirect('contato/index', 'refresh');
   	}

}