<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Noticias extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();

      $this->load->model('noticias_model', 'model');
    }

    function index($pag = 0){
      
      $this->load->library('pagination');
      $config['base_url'] = base_url('noticias/index/');
      $config['per_page'] = 12;
      $config['uri_segment'] = 3;
      $config['next_link'] = FALSE;
      $config['prev_link'] = FALSE;
      $config['num_links'] = 10;
      $config['first_link'] = FALSE;
      $config['last_link'] = FALSE; 
      $config['total_rows'] = $this->db->get('noticias')->num_rows();    
      $this->pagination->initialize($config);
      $data['paginacao'] = $this->pagination->create_links();
      $data['noticias'] = $this->model->pegarPaginado($config['per_page'], $pag, 'data', 'desc');

   		$this->load->view('noticias/index', $data);
   	}

    function detalhes($slug = false){
      
      if (!$slug)
        redirect('noticias/index');
      
      $data['detalhe'] = $this->db->get_where('noticias', array('slug' => $slug))->result();

      if(!isset($data['detalhe'][0]))
        redirect('noticias/index');

      $this->load->view('noticias/detalhes', $data); 
    }

}