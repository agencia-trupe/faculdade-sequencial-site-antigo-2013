<section>

	<div id="breadcrumbs">
		<a href="home" title="Página Inicial">home</a>&nbsp;&raquo;&nbsp;
		<a href="cursos" title="Cursos">cursos</a>&nbsp;&raquo;&nbsp;
		<a href="cursos/index/cursos_livres" title="Cursos Livres">cursos livres</a>&nbsp;&raquo;&nbsp;
		<a href="cursos/cursos_livres/<?=$detalhes->slug?>" title="<?=$detalhes->titulo?>"><?=minuscula($detalhes->titulo)?></a>
	</div>

	<h1>Cursos Livres</h1>

	<h2 class="topo-cursos livres">
		<div class="data">
			<?php if ($detalhes->data_inicio): ?>
				<div class="dia-e-mes"><?=dia($detalhes->data_inicio)?>&nbsp;<span class="mes"><?=mes($detalhes->data_inicio, true)?></span></div>
				<div class="ano"><?=ano($detalhes->data_inicio)?></div>
			<?php endif ?>			
		</div>
		<?=$detalhes->titulo?>,
	</h2>

	<div class="texto">
		<?php if ($detalhes->imagem): ?>
			<img src="_imgs/cursos/<?=$detalhes->imagem?>">
		<?php endif ?>
		<?= relativizaUrl($detalhes->texto) ?>
	</div>

	<?php if ($cursos): ?>

		<div class="titulo-outros">Confira outros cursos livres:</div>
		<div class="container">

			<?php $contador = 0 ?>
			<?php foreach ($cursos as $key => $value): ?>
				<a href="cursos/cursos_livres/<?=$value->slug?>" title="<?=$value->titulo?>" class="link-livres <?if($contador%2==0)echo" margem"?>">
					<div class="data">
						<?php if ($value->data_inicio): ?>
							<div class="dia-e-mes"><?=dia($value->data_inicio)?>&nbsp;<span class="mes"><?=mes($value->data_inicio, true)?></span></div>
							<div class="ano"><?=ano($value->data_inicio)?></div>
						<?php endif ?>
					</div>
					<div class="titulo">
						&raquo;&nbsp;<?=$value->titulo?>
					</div>
				</a>		
				<?php $contador++ ?>
			<?php endforeach ?>

		</div>

	<?php endif ?>

</section>