<section>
	
	<div id="breadcrumbs">
		<a href="home" title="Página Inicial">home</a>&nbsp;&raquo;&nbsp;
		<a href="cursos" title="Cursos">cursos</a>
		<?php if ($ativo): ?>
			<?
			switch ($ativo) {
				case 'graduacao':
					$extenso = "graduação";
					break;
				case 'pos_graduacao':
					$extenso = "pós-graduação";
					break;
				case 'extensao_universitaria':
					$extenso = "extensão universitária";
					break;
				case 'cursos_livres':
					$extenso = "cursos livres";
					break;
			}
			?>
			&nbsp;&raquo;&nbsp;<a href="cursos/index/<?=$ativo?>" title="<?=$extenso?>"><?=$extenso?></a>
		<?php endif ?>
	</div>

	<h1>Nossos Cursos<?if($ativo)echo" • ".ucfirst($extenso)?></h1>

	<p>A Faculdade Sequencial oferece cursos de:</p>

	<div class="container<?if($ativo)echo" ativo"?>" id="fixed_height">
		<a href="cursos/index/graduacao" title="Graduação" class="<?if($ativo=='graduacao')echo"ativo"?> link-tipos-cursos">Graduação</a>
		<a href="cursos/index/pos_graduacao" title="Pós-Graduação" class="<?if($ativo=='pos_graduacao')echo"ativo"?> link-tipos-cursos">Pós-Graduação</a>
		<a href="cursos/index/extensao_universitaria" title="Extensão Universitária" class="<?if($ativo=='extensao_universitaria')echo"ativo"?> link-tipos-cursos">Extensão Universitária</a>
		<a href="cursos/index/cursos_livres" title="Cursos Livres" class="<?if($ativo=='cursos_livres')echo"ativo"?> link-tipos-cursos nomargin">Cursos Livres</a>
	</div>

	<div class="container parent graduacao"<?php if ($ativo && $ativo != 'graduacao') echo " style='display:none;'" ?>>
		<h2>Graduação</h2>
		<p>
			A Sequencial pensou no seu futuro e selecionou dois cursos de excelência para a formação de novos profissionais com 
			mercado de trabalho crescente: Pedagogia e Enfermagem.
		</p>

		<p>
			Escolha seu curso e clique para saber mais:
		</p>

		<div class="container">
			<a href="cursos/graduacao/graduacao_em_pedagogia" title="Graduação em Pedagogia" id="chamada-pedagogia">Graduação em Pedagogia</a>
			<a href="cursos/graduacao/graduacao_em_enfermagem" title="Graduação em Enfermagem" id="chamada-enfermagem">Graduação em Enfermagem</a>
		</div>
	</div>


	<div class="container parent pos_graduacao" <?php if ($ativo && $ativo != 'pos_graduacao') echo " style='display:none;'" ?>>
		<h2>Pós-Graduação</h2>
		<p>
			Para tornar sua formação ainda mais completa a Sequencial oferece cursos de Pós-Graduação nas áreas de Educação e Saúde.
			Clique no curso desejado para saber mais:
		</p>
		<ul>
			<?php foreach ($pos as $key => $value): ?>
				<li><a href="cursos/pos-graduacao/<?=$value->slug?>" title="<?=$value->titulo?>" class="<?=$value->cor?>">&raquo;&nbsp;<?=$value->titulo?></a></li>
			<?php endforeach ?>
		</ul>
	</div>


	<div class="container parent extensao_universitaria"<?php if ($ativo && $ativo != 'extensao_universitaria') echo " style='display:none;'" ?>>
		<h2>Extensão Universitária</h2>
		<p>
			Para complementar a formação universitária a Faculdade Sequencial oferece cursos de Extensão, nas áreas de Educação e Saúde. Acompanhe aqui a divulgação de novos cursos e inscreva-se!
		</p>

		<?$contador = 0?>
		<div class="container">
			<?php foreach ($extensao as $key => $value): ?>

				<a href="cursos/extensao_universitaria/<?=$value->slug?>" title="<?=$value->titulo?>" class="link-extensao <?if($contador%2==0)echo" margem"?>">
					<div class="data">
						<?php if ($value->data_inicio): ?>
							<div class="dia-e-mes"><?=dia($value->data_inicio)?>&nbsp;<span class="mes"><?=mes($value->data_inicio, true)?></span></div>	
							<div class="ano"><?=ano($value->data_inicio)?></div>
						<?php endif ?>
					</div>
					<div class="titulo">
						&raquo;&nbsp;<?=$value->titulo?>
					</div>
				</a>

				<?$contador++?>

			<?php endforeach ?>
		</div>
	</div>

	
	<div class="container parent cursos_livres"<?php if ($ativo && $ativo != 'cursos_livres') echo " style='display:none;'" ?>>
		<h2>Cursos Livres</h2>
		<p>
			A Faculdade Sequencial promove com regularidade cursos livres com temas diversos. Alguns cursos são restritos a alunos e alguns são oferecidos aos alunos e à comunidade. Veja abaixo os cursos que serão realizados em breve e prepare-se:
		</p>

		<?$contador = 0?>
		<div class="container">
			<?php foreach ($livres as $key => $value): ?>

				<a href="cursos/cursos_livres/<?=$value->slug?>" title="<?=$value->titulo?>" class="link-livres <?if($contador%2==0)echo" margem"?>">
					<div class="data">
						<?php if ($value->data_inicio): ?>
							<div class="dia-e-mes"><?=dia($value->data_inicio)?>&nbsp;<span class="mes"><?=mes($value->data_inicio, true)?></span></div>
							<div class="ano"><?=ano($value->data_inicio)?></div>
						<?php endif ?>
					</div>
					<div class="titulo">
						&raquo;&nbsp;<?=$value->titulo?>
					</div>
				</a>

				<?$contador++?>

			<?php endforeach ?>
		</div>		
	</div>

</section>

<script defer>
	$('document').ready( function(){

		var hash = window.location.hash;

		if(hash){
			window.location = "cursos/index/"+hash.substring(1);
		}

	});
</script>