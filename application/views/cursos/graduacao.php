<section>
	
	<div id="breadcrumbs">
		<a href="home" title="Página Inicial">home</a>&nbsp;&raquo;&nbsp;
		<a href="cursos" title="Cursos">cursos</a>&nbsp;&raquo;&nbsp;
		<a href="cursos/index/graduacao" title="Cursos de Graduação">graduação</a>&nbsp;&raquo;&nbsp;
		<a href="cursos/graduacao/<?=$detalhes->slug?>" title="<?=$detalhes->titulo?>"><?=minuscula($detalhes->titulo)?></a>
	</div>

	<h1>Nossos Cursos &bull; Graduação</h1>

	<div id="topo-cursos" class="container">
		<img src="_imgs/cursos/<?=$detalhes->imagem_off?>" alt="<?=$detalhes->titulo?>">
		<div class="descricao" style="background-color:<?=$detalhes->cor?>">
			<h4>
				<?=$detalhes->titulo?>
			</h4>
			<h3>
				<?=$detalhes->subtitulo?>
			</h3>
			<p>
				<?=$detalhes->portaria?>
			</p>
			<?php if ($detalhes->edital): ?>
				<a href="_pdfs/$detalhes->edital" target="_blank" title="Download do Edital - <?=$detalhes->subtitulo?>">&raquo; DOWNLOAD DO EDITAL DE FORMAÇÃO DO CURSO</a>
			<?php endif ?>
		</div>
	</div>

	<?php if ($topicos): ?>

		<?php $ultima_cor = $topicos[0]->cor ?>

		<?php foreach ($topicos as $key => $value): ?>

			<?php if($ultima_cor != $value->cor) echo "<div class='espacamento'></div>" ?>

			<a class="lista-topicos <?=$value->cor?>" href="#" title="<?=$value->titulo?>"><?=$value->titulo?></a>

			<div class="detalhes-topicos">
				<?= relativizaUrl($value->texto) ?>
			</div>

			<?php $ultima_cor = $value->cor ?>

		<?php endforeach ?>
		
	<?php endif ?>

</section>

<script defer>
	$('document').ready( function(){

		$('.lista-topicos').click( function(e){
			e.preventDefault();

			var self = $(this);
			var texto = self.next('.detalhes-topicos');
			
			texto.slideToggle('normal');
			self.toggleClass('marcado');

		});

	});
</script>