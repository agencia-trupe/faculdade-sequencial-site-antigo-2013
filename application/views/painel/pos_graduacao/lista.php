<h1><?=$titulo?></h1>

<div id="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index')?>" class="lista active">Listar Cursos de Pós-Graduação</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/form')?>" class="add">Inserir Curso</a>
</div>


<?if($registros):?>

	<table>

		<thead>
			<tr>
				<th>Título</th>
				<th class="option-cell"></th>
				<!-- <th class="option-cell"></th> -->
			</tr>
		</thead>

		<? foreach ($registros as $key => $value): ?>

			<tr>
				<th><?=$value->titulo?></th>
				<td><a class="edit" href="<?=base_url('painel/'.$this->router->class.'/form/'.$value->id)?>">Editar</a></td>
				<!-- <td><a class="delete" href="<=base_url('painel/'.$this->router->class.'/excluir/'.$value->id)?>">Excluir</a></td> -->
			</tr>
			
		<? endforeach; ?>

	</table>

<?else:?>

	<h2 style="text-align:center;">Nenhum Curso Cadastrado</h2>

<?endif;?>
