<h1><?=$titulo?></h1>

<div id="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index')?>" class="lista">Listar Cursos de Graduação</a>
	<!-- <a href="<=base_url('painel/'.$this->router->class.'/form')?>" class="add active">Inserir Curso</a> -->
</div>

<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Título<br>
			<input type="text" name="titulo" value="<?=$registro->titulo?>">
		</label>

		<label>Sub-Título<br>
			<input type="text" name="subtitulo" value="<?=$registro->subtitulo?>">
		</label>

		<label>Portaria<br>
			<textarea name="portaria" style="height:60px; resize:none;"><?=$registro->portaria?></textarea>
		</label>

		<label>Edital<br>
			<a href="_pdfs/<?=$registro->edital?>" title="Visualizar o Arquivo" target="_blank"><?=$registro->edital?></a><br>
			<input type="file" name="userfile">
		</label>

		<label>Imagem ON<br>
			<?php if ($registro->imagem_on): ?>
				<img src="_imgs/cursos/<?=$registro->imagem_on?>"><br>
			<?php endif ?>
			<input type="file" name="imagem_on">
		</label>

		<label>Imagem OFF<br>
			<?php if ($registro->imagem_off): ?>
				<img src="_imgs/cursos/<?=$registro->imagem_off?>"><br>
			<?php endif ?>
			<input type="file" name="imagem_off">
		</label>

		<br><br>

		<h2 id="inicio-topicos">Tópicos</h2>

		<div style="padding-left:15px;">

			<?php if (!$topicos): ?>
				<input id="geraPadrao" class="botao" type="button" value="GERAR TÓPICOS PADRÃO"><br><br>
			<?php endif ?>

			Novo Tópico<br>
			<label>Título<br>
			<input type="text" name="topico" id="topico-tit"></label>

			<label>Texto<br>
			<textarea class="topico" id="topico-txt"></textarea></label>

			<label>Cor<br>
			<select id="topico-cor">
				<option value=""></option>
				<option value="azul">azul</option>
				<option value="laranja">laranja</option>
			</select>
			</label>

			<input type="hidden" id="edicao" value="">

			<div id="sucesso" class="mensagem resposta"></div>

			<input type="submit" value="GRAVAR TÓPICO" id="gravar-topico">

		</div>

		<br><br>

		<table id="topic-table">
			<thead>
				<tr>
					<th>Título</th>
					<th>Texto</th>
					<th>Cor</th>
					<th></th>
					<th></th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php if ($topicos): ?>
					<?php foreach ($topicos as $key => $value): ?>
						<tr id="row_<?=$value->id?>">
							<td><?=strip_tags($value->titulo)?></td>
							<td><?=word_limiter(strip_tags($value->texto), 10)?></td>
							<td>
							<?
								if($value->cor == 'azul'){
									echo "<span class='fundoazul'>AZUL</span>";
								}else{
									echo "<span class='fundolaranja'>LARANJA</span>";
								}
							?>
							</td>
							<td><a href="#" data-id="<?=$value->id?>" class="edit">editar</a></td>
							<td><a href="#" data-id="<?=$value->id?>" class="delete">excluir</a></td>
							<td><a href="#" data-id="<?=$value->id?>" class="move">mover</a></td>
						</tr>
					<?php endforeach ?>
				<?php endif ?>
			</tbody>
		</table>

		<br><br>

		<input type="submit" value="ALTERAR"> <input type="button" class="voltar" value="VOLTAR">

	</form>


<script defer>

	function atualizaTabela(){
		var html = '';
		$.post(BASE+'/ajax/pegaTopicos', { id_textos : '<?=$registro->id?>', tabela : 'graduacao_topicos'}, function(retorno){
			retorno = JSON.parse(retorno);
			$('#topic-table tbody').slideUp('slow', function(){
				$('#topico-tit').val('');
				tinyMCE.get('topico-txt').setContent('');
				$('#topico-cor').val('');
				$(this).html('');
				retorno.map( function(self){
					html += "<tr id='row_"+self.id+"'>";
						html += "<td>"+self.titulo.replace(/(<([^>]+)>)/ig,"")+"</td>";
						html += "<td>"+self.texto.replace(/(<([^>]+)>)/ig,"")+"</td>";
						html += "<td>";
						html += (self.cor == 'azul') ? "<span class='fundoazul'>AZUL</span>" : "<span class='fundolaranja'>LARANJA</span>";
						html += "</td>";
						html += "<td><a href='#' data-id='"+self.id+"' class='edit'>editar</a></td>";
						html += "<td><a href='#' data-id='"+self.id+"' class='delete'>excluir</a></td>";
						html += "<td><a href='#' data-id='"+self.id+"' class='move'>mover</a></td>";
					html += "</tr>";
				});
				$(this).html(html).slideDown('slow');
				$('body').animate({ scrollTop : $("#topic-table").offset().top});
			})
		});
	}

	$('document').ready( function(){

		$('#gravar-topico').click( function(e){
			e.preventDefault();

			var tit  = $('#topico-tit').val();
			var text = tinyMCE.get('topico-txt').getContent();
			var cor = $('#topico-cor').val();

			if(tit == '' || text == '')
				return false;

			if($('#edicao').val() == ''){
				$.post(BASE+'/ajax/insereTopico', {tabela : 'graduacao_topicos', id_textos : '<?=$registro->id?>', titulo : tit, texto : text, cor : cor}, function(){
					atualizaTabela();
					$('.resposta').html('Tópico inserido com sucesso').show(0, function(){
						$('.resposta').delay(2000).hide(150);
					})
				});
			}else{
				$.post(BASE+'/ajax/alteraTopico', {tabela : 'graduacao_topicos', id_topico : $('#edicao').val(), titulo : tit, texto : text, cor : cor}, function(){
					atualizaTabela();
					$('#edicao').val('');
					$('.resposta').html('Tópico alterado com sucesso').show(0, function(){
						$('.resposta').delay(2000).hide(150);
					})
				});
			}
		});

		$('#geraPadrao').live('click', function(){
			$.post(BASE+'/ajax/geraTopicosPadrao', { id_textos : '<?=$registro->id?>' , tabela : 'graduacao_topicos' }, function(){
				atualizaTabela();
				$('#geraPadrao').hide(0, function(){
					$('#geraPadrao').remove();
				});
			});
		});

		$('#topic-table .delete').unbind();

		$('#topic-table .delete').live('click', function(e){
			e.preventDefault();

			var id = $(this).attr('data-id');

			if(confirm('Deseja Excluir o Tópico?')){
				$.post(BASE+'/ajax/excluiTopico', { id : id, tabela : 'graduacao_topicos'}, function(retorno){
					atualizaTabela();
				});
			}
		});

		$('#topic-table .edit').live('click', function(e){
			e.preventDefault();

			var id = $(this).attr('data-id');

			$.post(BASE+'/ajax/pegarTopico', {id_topico : id, tabela : 'graduacao_topicos'}, function(retorno){
				retorno = JSON.parse(retorno);
				$('#edicao').val(retorno[0].id);
				$('#topico-tit').val(retorno[0].titulo);
				$('#topico-cor').val(retorno[0].cor);
				tinyMCE.get('topico-txt').setContent(retorno[0].texto);
				$('body').animate({ scrollTop : $("#inicio-topicos").offset().top});
			});

		});

	    $("#topic-table tbody").sortable({
	        update : function () {
	            serial = [];
	            $('#topic-table tbody').children('tr').each(function(idx, elm) {
	                serial.push(elm.id.split('_')[1])
	            });
	            $.post(BASE+'/ajax/gravaOrdem', { data : serial , tabela : 'graduacao_topicos'}, function(retorno){
	            	console.log(retorno);
	            });
	        },
	        helper: function(e, ui) {
				ui.children().each(function() {
					$(this).width($(this).width());
				});
				return ui;
			},
			handle : $('.move')
	    }).disableSelection();

	});
</script>

<style type="text/css">
	#topic-table tbody tr{
		width:100%;
	}
</style>


<?else: ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/inserir')?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Título<br>
			<input type="text" name="titulo">
		</label>

		<label>Sub-Título<br>
			<input type="text" name="subtitulo">
		</label>

		<label>Portaria<br>
			<textarea name="portaria" style="height:60px; resize:none;"></textarea>
		</label>

		<label>Edital<br>
			<input type="file" name="userfile">
		</label>

		<label>Imagem ON<br>
			<input type="file" name="imagem_on">
		</label>

		<label>Imagem OFF<br>
			<input type="file" name="imagem_off">
		</label>

		<input type="submit" value="INSERIR"> <input type="button" class="voltar" value="VOLTAR">
	</form>

<?endif ?>