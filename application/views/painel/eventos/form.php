<h1><?=$titulo?></h1>

<div id="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index')?>" class="lista">Listar Eventos</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/form')?>" class="add active">Inserir Evento</a>
</div>

<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Título<br>
			<input type="text" name="titulo" required value="<?=$registro->titulo?>">
		</label>

		<label>Data<br>
			<input type="text" name="data" id="datepicker" required value="<?=formataData($registro->data, 'mysql2br')?>">
		</label>

		<label>Detalhe de Data<br>
			<input type="text" name="data_detalhe" value="<?=$registro->data_detalhe?>">
		</label>

		<label>Imagem<br>
			<?php if ($registro->imagem): ?>
				<img src="_imgs/eventos/<?=$registro->imagem?>"><br>
			<?php endif ?>
			<input type="file" name="userfile">
		</label>

		<label>
			Texto<br>
			<textarea class="comimagem" name="texto"><?=$registro->texto?></textarea>
		</label>

		<label>Olho<br>
			<textarea name="olho" style="height:150px; width:400px; resize:none;"><?=$registro->olho?></textarea>
		</label>		

		<input type="submit" value="ALTERAR"> <input type="button" class="voltar" value="VOLTAR">
	</form>
	
<?else: ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/inserir')?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Título<br>
			<input type="text" name="titulo" required>
		</label>

		<label>Data<br>
			<input type="text" name="data" id="datepicker" required>
		</label>

		<label>Detalhe de Data<br>
			<input type="text" name="data_detalhe">
		</label>

		<label>Imagem<br>
			<input type="file" name="userfile">
		</label>

		<label>
			Texto<br>
			<textarea class="comimagem" name="texto"></textarea>
		</label>

		<label>Olho<br>
			<textarea name="olho" style="height:150px; width:400px; resize:none;"></textarea>
		</label>

		<input type="submit" value="INSERIR"> <input type="button" class="voltar" value="VOLTAR">
	</form>

<?endif ?>