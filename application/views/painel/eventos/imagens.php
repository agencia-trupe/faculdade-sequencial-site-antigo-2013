<h1><?=$titulo?></h1>

<div id="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index')?>" class="lista">Listar Eventos</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/form')?>" class="add">Inserir Evento</a>
</div>

<?php if (isset($registro) && $registro): ?>

	<form id="form-img" method="post" action="<?='painel/'.$this->router->class.'/editarImagem/'.$registro->id?>" enctype="multipart/form-data">

		<label>
			Imagem Atual<br>
			<img src="_imgs/eventos/<?=$registro->imagem?>"><br>
			Substituir Imagem<br>
			<input type="file" id="input-img" name="userfile">
		</label>

		<input type="hidden" name="id_parent" value="<?=$parent->id?>">
		 
		<input type="submit" value="ALTERAR"> <input type="button" class="voltar" value="VOLTAR">
	</form>
	
<?php else: ?>

	<form method="post" id="form-img" action="<?='painel/'.$this->router->class.'/inserirImagem'?>" enctype="multipart/form-data">

		<label>
			Imagem<br>
			<input type="file" id="input-img" name="userfile">
		</label>

		<input type="hidden" name="id_parent" value="<?=$parent->id?>">
		
		<input type="submit" value="INSERIR"> <input type="button" class="voltar" value="VOLTAR">
	</form>
	
<?php endif ?>

<br>

<?if($imagens):?>

	<table>

		<thead>
			<tr>
				<th>Imagens</th>
				<th class="option-cell"></th>
			</tr>
		</thead>	

	</table>

	<? foreach ($imagens as $key => $value): ?>

		<div class="thumb-cell">
			<td><img src="_imgs/eventos/thumbs/<?=$value->imagem?>"></td>
			<td><a class="edit" href="<?=base_url('painel/'.$this->router->class.'/imagens/'.$parent->id.'/'.$value->id)?>">Editar</a></td>
			<td><a class="delete" href="<?=base_url('painel/'.$this->router->class.'/excluirImagem/'.$value->id.'/'.$parent->id)?>">Excluir</a></td>
		</div>
		
	<? endforeach; ?>

		

	

<?else:?>

	<h2>Nenhuma imagem cadastrada!</h2>

<?endif;?>