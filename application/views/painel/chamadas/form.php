<h1><?=$titulo?></h1>

<div id="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index')?>" class="lista">Listar Chamadas</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/form')?>" class="add active">Inserir Chamada</a>
</div>

<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Texto<br>
			<textarea name="titulo" style="height:150px; resize:none;"><?=$registro->titulo?></textarea>
		</label>

		<label>Destino<br>
			<input type="text" name="destino" maxlength="250" value="<?=$registro->destino?>">
		</label>

		<input type="submit" value="ALTERAR"> <input type="button" class="voltar" value="VOLTAR">
	</form>
	
<?else: ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/inserir')?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Texto<br>
			<textarea name="titulo" style="height:150px; resize:none;"></textarea>
		</label>

		<label>Destino<br>
			<input type="text" name="destino" maxlength="250">
		</label>

		<input type="submit" value="INSERIR"> <input type="button" class="voltar" value="VOLTAR">
	</form>

<?endif ?>