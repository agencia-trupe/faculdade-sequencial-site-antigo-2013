<h1><?=$titulo?></h1>

<div id="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index')?>" class="lista">Listar Slides</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/form')?>" class="add active">Inserir Slides</a>
</div>

<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Título<br>
			<input type="text" name="titulo" maxlength="140" value="<?=$registro->titulo?>">
		</label>

		<label>Imagem<br>
			<img src="_imgs/slides/<?=$registro->imagem?>"><br>
		<input type="file" name="userfile"></label>

		<label>Destino do link<br>
			<input type="text" name="destino" maxlength="250" value="<?=$registro->destino?>">
		</label>

		<input type="submit" value="ALTERAR"> <input type="button" class="voltar" value="VOLTAR">
	</form>
	
<?else: ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/inserir')?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Título<br>
			<input type="text" name="titulo" maxlength="140">
		</label>

		<label>Imagem<br>
		<input type="file" name="userfile" required></label>

		<label>Destino do link<br>
			<input type="text" name="destino" maxlength="250">
		</label>

		<input type="submit" value="INSERIR"> <input type="button" class="voltar" value="VOLTAR">
	</form>

<?endif ?>