<h1><?=$titulo?></h1>

<div id="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index')?>" class="lista">Listar Cursos de Extensão</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/form')?>" class="add active">Inserir Curso de Extensão</a>
</div>

<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Título<br>
			<input type="text" name="titulo" maxlength="250" value="<?=$registro->titulo?>">
		</label>

		<label>Data de Início<br>
			<input type="text" name="data_inicio" id="datepicker" value="<?=formataData($registro->data_inicio, 'mysql2br')?>">
		</label>

		<label>Detalhe de Data<br>
			<input type="text" name="data_detalhe" value="<?=$registro->data_detalhe?>">
		</label>

		<label>Imagem<br>
			<?php if ($registro->imagem): ?>
				<img src="_imgs/cursos/<?=$registro->imagem?>"><br>
			<?php endif ?>
			<input type="file" name="userfile">
		</label>		

		<label>Descrição<br>
			<textarea name="texto" class="comimagem"><?=$registro->texto?></textarea>
		</label>

		<input type="submit" value="ALTERAR"> <input type="button" class="voltar" value="VOLTAR">
	</form>
	
<?else: ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/inserir')?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Título<br>
			<input type="text" name="titulo" maxlength="250">
		</label>

		<label>Data de Início<br>
			<input type="text" name="data_inicio" id="datepicker">
		</label>

		<label>Detalhe de Data<br>
			<input type="text" name="data_detalhe">
		</label>

		<label>Imagem<br>
			<input type="file" name="userfile">
		</label>

		<label>Descrição<br>
			<textarea name="texto" class="comimagem"></textarea>
		</label>

		<input type="submit" value="INSERIR"> <input type="button" class="voltar" value="VOLTAR">
	</form>

<?endif ?>