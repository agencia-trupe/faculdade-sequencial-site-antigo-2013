<h1><?=$titulo?></h1>

<div id="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index')?>" class="lista active">Listar Novidades</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/form')?>" class="add">Inserir Novidade</a>
</div>


<?if($registros):?>

	<table>

		<thead>
			<tr>
				<th>Texto</th>
				<th class="option-cell"></th>
				<th class="option-cell"></th>
			</tr>
		</thead>

		<? foreach ($registros as $key => $value): ?>

			<tr>
				<th><?=wordwrap($value->texto, 15)?></th>
				<td><a class="edit" href="<?=base_url('painel/'.$this->router->class.'/form/'.$value->id)?>">Editar</a></td>
				<td><a class="delete" href="<?=base_url('painel/'.$this->router->class.'/excluir/'.$value->id)?>">Excluir</a></td>
			</tr>
			
		<? endforeach; ?>

	</table>

<?else:?>

	<h2 style="text-align:center;">Nenhum Item do Mural Cadastrado</h2>

<?endif;?>
