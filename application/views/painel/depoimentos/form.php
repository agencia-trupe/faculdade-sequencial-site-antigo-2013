<h1><?=$titulo?></h1>

<div id="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index')?>" class="lista">Listar Depoimentos</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/form')?>" class="add active">Inserir Depoimento</a>
</div>

<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Texto<br>
			<textarea name="texto" style="height:150px; resize:none;"><?=$registro->texto?></textarea>
		</label>

		<label>Autor<br>
			<textarea name="autor" style="height:75px; resize:none;"><?=$registro->autor?></textarea>
		</label>

		<label>Imagem<br>
			<?php if ($registro->imagem): ?>
				<img src="_imgs/depoimentos/<?=$registro->imagem?>"><br>
			<?php endif ?>
			<input type="file" name="userfile">
		</label>

		<input type="submit" value="ALTERAR"> <input type="button" class="voltar" value="VOLTAR">
	</form>
	
<?else: ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/inserir')?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Texto<br>
			<textarea name="texto" style="height:150px; resize:none;"></textarea>
		</label>

		<label>Autor<br>
			<textarea name="autor" style="height:75px; resize:none;"></textarea>
		</label>

		<label>Imagem<br>
			<input type="file" name="userfile">
		</label>

		<input type="submit" value="INSERIR"> <input type="button" class="voltar" value="VOLTAR">
	</form>

<?endif ?>