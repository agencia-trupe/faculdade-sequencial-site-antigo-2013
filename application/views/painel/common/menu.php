
<!-- Shell -->
<div id="shell">
	
	<!-- Header -->
	<div id="header">
		<h1><a href="<?=base_url('painel/home')?>">Painel - <?=CLIENTE?></a></h1>
		<div class="right">
			<p>Bem vindo, <strong><?=$this->session->userdata('username')?></strong></p>
			<p class="small-nav"><a href="<?=base_url('painel/usuarios')?>" <?if($this->router->class=='usuarios')echo " class='active'"?>>Usuários</a> / <a href="<?=base_url('painel/home/logout')?>">Log Out</a></p>
		</div>
	</div>
	<!-- End Header -->
	
	<!-- Navigation -->
	<div id="navigation">
		<ul>
		    <li><a href="<?=base_url('painel/home')?>" <?if($this->router->class=='home')echo " class='active'"?>><span>Início</span></a></li>
		    <li><a href="<?=base_url('painel/slides')?>" <?if($this->router->class=='slides')echo " class='active'"?>><span>Slides</span></a></li>
		    <li><a href="<?=base_url('painel/mural')?>" <?if($this->router->class=='mural')echo " class='active'"?>><span>Mural</span></a></li>
		    <li><a href="<?=base_url('painel/novidades')?>" <?if($this->router->class=='novidades')echo " class='active'"?>><span>Novidades</span></a></li>
		    <li><a href="<?=base_url('painel/depoimentos')?>" <?if($this->router->class=='depoimentos')echo " class='active'"?>><span>Depoimentos</span></a></li>
		    <li><a href="<?=base_url('painel/chamadas')?>" <?if($this->router->class=='chamadas')echo " class='active'"?>><span>Chamadas</span></a></li>
		    <li><a href="<?=base_url('painel/destaques')?>" <?if($this->router->class=='destaques')echo " class='active'"?>><span>Destaques</span></a></li>
		    <li><a href="<?=base_url('painel/graduacao')?>" <?if($this->router->class=='graduacao')echo " class='active'"?>><span>Graduação</span></a></li>
		    <li><a href="<?=base_url('painel/pos_graduacao')?>" <?if($this->router->class=='pos_graduacao')echo " class='active'"?>><span>Pós-Graduação</span></a></li>
		    <li><a href="<?=base_url('painel/cursos_livres')?>" <?if($this->router->class=='cursos_livres')echo " class='active'"?>><span>Cursos Livres</span></a></li>
		    <li><a href="<?=base_url('painel/cursos_extensao')?>" <?if($this->router->class=='cursos_extensao')echo " class='active'"?>><span>Cursos de Extensão</span></a></li>
		    <li><a href="<?=base_url('painel/noticias')?>" <?if($this->router->class=='noticias')echo " class='active'"?>><span>Notícias</span></a></li>
		    <li><a href="<?=base_url('painel/eventos')?>" <?if($this->router->class=='eventos')echo " class='active'"?>><span>Eventos</span></a></li>
		</ul>
	</div>
	<!-- End Navigation -->
	
	<!-- Content -->
	<div id="content">

	<?if(isset($mostrarerro_mensagem) && $mostrarerro_mensagem):?>
		<div class="mensagem" id="erro"><?=$mostrarerro_mensagem?></div>
	<?elseif(isset($mostrarsucesso_mensagem) && $mostrarsucesso_mensagem):?>
		<div class="mensagem" id="sucesso"><?=$mostrarsucesso_mensagem?></div>
	<?endif;?>