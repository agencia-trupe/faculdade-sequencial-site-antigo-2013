<section>

	<div id="breadcrumbs">
		<a href="home" title="Página Inicial">home</a>&nbsp;&raquo;&nbsp;
		<a href="contato" title="Contato">contato</a>
	</div>
	
	<h1>Contato</h1>

	<div id="form">

		<h2>FALE CONOSCO</h2>

		<?php if ($this->session->flashdata('enviado')): ?>

			<div class="mensagem">
				Sua mensagem foi enviada com sucesso!<br>
				Responderemos assim que possível.
			</div>
			
		<?php else: ?>

			<form method="post" action="contato/enviar">

				<input type="text" name="nome" placeholder="nome" required id="input-nome">

				<input type="email" name="email" placeholder="email" required id="input-email">

				<input type="text" name="telefone" placeholder="telefone" required id="input-telefone">

				<textarea name="mensagem" placeholder="mensagem" required id="input-mensagem"></textarea>

				<input type="submit" value="ENVIAR">

			</form>

		<?php endif ?>
		
	</div>

	<p class="atendimento">
		CENTRAL DE ATENDIMENTO:<br>
		<span class="telefone">11 3371-2828</span>
	</p>

	<p class="endereco">
		Rua Engº Aluisio Marques, S/N<br>
		Parque Maria Helena<br>
		Campo Limpo / Capão Redondo<br>
		05854-110 - São Paulo - SP
	</p>

	<p>
		Em frente à estação do metrô Capão Redondo | Altura do 3700 da Estrada de Itapecerica
	</p>

</section>

<script defer>
	$('document').ready( function(){

		$('form').submit( function(){

			if($('#input-nome').val() == '' || $('#input-nome').val() == 'nome'){
				alert('Informe seu nome.');
				return false;
			}
			if($('#input-email').val() == '' || $('#input-email').val() == 'e-mail'){
				alert('Informe seu e-mail.');
				return false;
			}
			if($('#input-telefone').val() == '' || $('#input-telefone').val() == 'telefone'){
				alert('Informe um telefone para contato.');
				return false;
			}
			if($('#input-mensagem').val() == '' || $('#input-mensagem').val() == 'mensagem'){
				alert('Informe sua mensagem.');
				return false;
			}

		});

	});
</script>