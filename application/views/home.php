	<div id="slideshow">

		<div id="animate">
			<?php if ($slides): ?>
				<?php foreach ($slides as $key => $value): ?>
					<a href="<?=$value->destino?>" title="<?=$value->titulo?>" <?if($key>0)echo"style='display:none;'"?>>
						<img src="_imgs/slides/<?=$value->imagem?>" alt="<?=$value->titulo?>">
					</a>
				<?php endforeach ?>
			<?php endif ?>
		</div>

		<div id="bullets">
			
		</div>

	</div>


	<div class="faixa-branca">
		<div class="centro">

			<div class="coluna-direita">
				
				<?php if ($mural): ?>

					<div id="mural">

						<h1>MURAL</h1>

						<?php foreach ($mural as $key => $value): ?>
							<a title="<?=word_limiter($value->texto, 5)?>" href="<?=$value->destino?>">
								<div class="texto">
									<?=nl2br($value->texto)?>	
								</div>
							</a>
						<?php endforeach ?>

					</div>

				<?php endif ?>

				<?php if ($depoimentos): ?>
					
					<div id="depoimentos">

						<h1>DEPOIMENTOS</h1>
						
						<?php foreach ($depoimentos as $key => $value): ?>
							<div class="depoimento" style="background:url('_imgs/depoimentos/<?=$value->imagem?>') 95% bottom no-repeat;">
								<div class="texto">
									<?=nl2br($value->texto)?>
								</div>
								<div class="autor">
									<?=$value->autor?>
								</div>
							</div>
						<?php endforeach ?>

					</div>

				<?php endif ?>

			</div>		

			<div class="coluna-esquerda">
				
				<div class="links-box">
					<a href="cursos/graduacao" title="Graduação">Graduação</a>
					<a href="cursos/pos-graduacao" title="Pós-Graduação">Pós-Graduação</a>
					<a href="cursos/extensao-universitaria" title="Extensão">Extensão</a>
					<a href="cursos/cursos-livres" class="ultimo" title="Cursos Livres">Cursos Livres</a>
				</div>

				<h1>NOSSOS CURSOS DE GRADUAÇÃO:</h1>

				<a href="cursos/graduacao/graduacao_em_pedagogia" title="Graduação em Pedagogia" id="chamada-pedagogia">
					Graduação em Pedagogia
				</a>

				<a href="cursos/graduacao/graduacao_em_enfermagem" title="Graduação em Enfermagem" id="chamada-enfermagem">
					Graduação em Enfermagem
				</a>

			</div>
			
		</div>
	</div>

<!-- FIM DA DIV MAIN -->
</div>

<div class="faixa-azul-clara">
	<div class="centro">
		
		<div class="faixa-esquerda">
			<h1>MAIS <img src="_imgs/layout/bhome-sinal-mais.png" alt="Mais"> NOVIDADES</h1>

			<?php if ($novidades): ?>
				<ul>
					<?php foreach ($novidades as $key => $value): ?>
						<li><a href="<?=$value->destino?>" title="<?=$value->texto?>"><?=$value->texto?></a></li>	
					<?php endforeach ?>
				</ul>
			<?php endif ?>
		</div>

	</div>	
</div>

<div class="faixa-azul-escura">
	<div class="centro">

		<div class="coluna-esquerda">

			<h1>COMO INGRESSAR</h1>

			<ul>
				<li><a href="bolsas-de-estudo" title="BOLSAS DE ESTUDO - ATÉ 100% DE DESCONTO" class="vermelho">&raquo;&nbsp;&nbsp;DESCONTOS ESPECIAIS DE ATÉ 100%</a></li>
				<li><a href="processo-seletivo" title="VESTIBULAR TRADICIONAL" class="laranja">&raquo;&nbsp;&nbsp;VESTIBULAR TRADICIONAL</a></li>
				<li><a href="como-ingressar/#enem" title="ANÁLISE DA NOTA DO ENEM" class="amarelo">&raquo;&nbsp;&nbsp;ANÁLISE DA NOTA DO ENEM</a></li>
				<li><a href="como-ingressar/#transferencia" title="TRANSFERÊNCIA" class="verde-claro">&raquo;&nbsp;&nbsp;TRANSFERÊNCIA</a></li>
				<li><a href="financiamentos/#fies" title="FIES - FUNDO DE FINANCIAMENTO ESTUDANTIL" class="verde-medio">&raquo;&nbsp;&nbsp;FIES - FUNDO DE FINANCIAMENTO ESTUDANTIL</a></li>
				<li><a href="bolsas-de-estudo/#prouni" title="PROUNI - PROGRAMA UNIVERSIDADE PARA TODOS" class="verde-escuro">&raquo;&nbsp;&nbsp;PROUNI - PROGRAMA UNIVERSIDADE PARA TODOS</a></li>
				<li><a href="bolsas-de-estudo/#escola" title="PROGRAMA ESCOLA DA FAMÍLIA" class="azul">&raquo;&nbsp;&nbsp;PROGRAMA ESCOLA DA FAMÍLIA</a></li>
			</ul>
		</div>

	</div>	
</div>

<div class="faixa-branca-final">
	
	<div class="centro">
		
		<?php if ($chamadas): ?>
			
			<?php $classe = "laranja" ?>

			<?php foreach ($chamadas as $key => $value): ?>

				<a href="<?=$value->destino?>" class="chamada <?=$classe?>" title="<?=$value->titulo?>">
					<?=nl2br($value->titulo)?>
				</a>

				<?php $classe = "azul" ?>

			<?php endforeach ?>

		<?php endif ?>

		<div id="facebook">
			<div class="fb-like-box" data-href="https://www.facebook.com/pages/Faculdade-Sequencial/127798883992592" data-width="340" data-height="280" data-show-faces="true" data-stream="false" data-header="false"></div>
		</div>

	</div>

</div>