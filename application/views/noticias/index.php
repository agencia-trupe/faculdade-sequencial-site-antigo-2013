<section>

	<div id="breadcrumbs">
		<a href="home" title="Página Inicial">home</a>&nbsp;&raquo;&nbsp;
		<a href="noticias" title="Notícias">notícias</a>
	</div>

	<h1>Notícias</h1>

	<?php if ($noticias): ?>

		<?php foreach ($noticias as $key => $value): ?>
			
			<a class="link-noticias" href="noticias/detalhes/<?=$value->slug?>" title="<?=$value->titulo?>">
				<?php if ($value->imagem): ?>
					<img src="_imgs/noticias/thumbs/<?=$value->imagem?>" alt="<?=$value->titulo?>">
				<?php endif ?>
				<div class="data"><?=dia($value->data).' '.mes($value->data, TRUE).' '.ano($value->data)?></div>
				<h2><?=$value->titulo?></h2>
			</a>

		<?php endforeach ?>

		<?php if ($paginacao): ?>
			<div id="paginacao"><?php echo $paginacao ?></div>
		<?php endif ?>
		
	<?php else: ?>
		
		<h2>Nenhuma Notícia Cadastrada</h2>

	<?php endif ?>

</section>