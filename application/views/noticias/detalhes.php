<section>

	<div id="breadcrumbs">
		<a href="home" title="Página Inicial">home</a>&nbsp;&raquo;&nbsp;
		<a href="noticias/index" title="Notícias">notícias</a>&nbsp;&raquo;&nbsp;
		<a href="noticias/detalhes/<?=$detalhe[0]->slug?>" title="<?=$detalhe[0]->titulo?>"><?=word_limiter(minuscula($detalhe[0]->titulo), 6)?></a>
	</div>

	<a class="link-voltar" href="javascript:window.history.back(-2)" title="Voltar">&laquo; voltar para a home de notícias</a>

	<h1>Notícias</h1>

	<div class="detalhe">

		<?php if (isset($detalhe[0]) && $detalhe[0]): ?>

			<h2>
				<div class="data"><?=dia($detalhe[0]->data).' '.mes($detalhe[0]->data, TRUE).' '.ano($detalhe[0]->data)?></div>
				<?=$detalhe[0]->titulo?>
			</h2>
			
			<?php if ($detalhe[0]->imagem): ?>
				<div class="imagem">
					<img class="imagem-principal" src="_imgs/noticias/<?=$detalhe[0]->imagem?>" alt="<?=$detalhe[0]->titulo?>">
				</div>
			<?php endif ?>
			
			<?php if ($detalhe[0]->olho): ?>
				<div class="olho">
					<?=$detalhe[0]->olho?>
				</div>	
			<?php else: ?>
				<div class="espacamento"></div>
			<?php endif ?>
			

			<?= relativizaUrl($detalhe[0]->texto) ?>
		
		<?php endif; ?>

	</div>

	<a href="javascript:window.history.back(-2)" title="Voltar" class="link-voltar-inferior">&laquo; voltar para a home de notícias</a>

</section>