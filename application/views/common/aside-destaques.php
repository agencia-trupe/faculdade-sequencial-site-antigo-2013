<aside>

	<?php if ($destaques): ?>
		
		<h2>ASSUNTOS EM DESTAQUE:</h2>

		<ul>
			<?php foreach ($destaques as $key => $value): ?>
				<li>
					<a href="<?=$value->destino?>" title="<?=$value->titulo?>">
						<h3><?=$value->titulo?></h3>
						<p><?=$value->texto?></p>
					</a>
				</li>
			<?php endforeach ?>
		</ul>

	<?php endif ?>
	
</aside>