<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="pt-BR"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="pt-BR"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="pt-BR"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="pt-BR"> <!--<![endif]-->
<head>
  <meta charset="utf-8">

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title>Faculdade Sequencial - Cursos de graduação em Enfermagem e Cursos de graduação em Pedagogia</title>
  <meta name="description" content="Faculdade Sequencial. Cursos superiores de Enfermagem e Pedagogia. São Paulo, SP." />
  <meta name="keywords" content="faculdade, sequencial, faculdade sequencial, enfermagem, pedagogia, faculdade de enfermagem, faculdade de pedagogia, bacharelado em enfermagem, licenciatura em pedagogia, processo seletivo, vestibular," />
  <meta name="robots" content="index, follow" />
  <meta name="author" content="Trupe Design" />
  <meta name="copyright" content="2012 Trupe Design" />

  <meta name="viewport" content="width=device-width,initial-scale=1">

  <meta property="og:title" content="Faculdade Sequencial - Cursos de graduação em Enfermagem e Cursos de graduação em Pedagogia"/>
  <meta property="og:site_name" content="Faculdade Sequencial"/>
  <meta property="og:type" content="website"/>
  <meta property="og:image" content="<?=base_url('_imgs/layout/fb.jpg')?>"/>
  <meta property="og:url" content="<?=base_url()?>"/>
  <meta property="og:description" content="Faculdade Sequencial. Cursos superiores de Enfermagem e Pedagogia. São Paulo, SP."/>

  <base href="<?= base_url() ?>">
  <script> var BASE = '<?= base_url() ?>'</script>

  <?CSS(array('reset', 'base', 'fontface/stylesheet', 'fancybox/fancybox',$this->router->class, $load_css))?>


  <?if(ENVIRONMENT == 'development'):?>

    <?JS(array('modernizr-2.0.6.min', 'less-1.3.0.min', 'jquery-1.8.0.min', $this->router->class, $load_js))?>

  <?else:?>

    <?JS(array('modernizr-2.0.6.min', $this->router->class, $load_js))?>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/jquery-1.8.0.min.js"><\/script>')</script>

  <?endif;?>

</head>
<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1&appId=393627327333856";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>