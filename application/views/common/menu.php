<header class="centro">

	<a id="link-home" href="<?=base_url()?>" title="Página Inicial"><img src="_imgs/layout/marca.png" alt="Faculdade Sequencial"></a>

	<div id="like-wrapper">
		<div class="fb-like" data-href="https://www.facebook.com/faculdade.sequencial" data-width="125" data-layout="button_count" data-show-faces="false" data-send="false"></div>
	</div>

	<nav id="menu-horizontal">
		<ul id="superior">
			<li><a href="<?=base_url()?>" title="Página Inicial" id="mn-home" <?if($this->router->class=='home')echo" class='ativo'"?>>HOME</a></li>
			<li><a href="faculdade" title="A Faculdade" id="mn-faculdade" <?if($this->router->class=='faculdade')echo" class='ativo'"?>>A FACULDADE</a></li>
			<li><a href="localizacao" title="Localização" id="mn-localizacao" <?if($this->router->class=='localizacao')echo" class='ativo'"?>>LOCALIZAÇÃO</a></li>
			<li><a href="contato" title="Contato" id="mn-contato" <?if($this->router->class=='contato')echo" class='ativo'"?>>CONTATO</a></li>
		</ul>
		<ul id="inferior">
			<li><a href="cursos" title="Cursos" id="mn-cursos" <?if($this->router->class=='cursos')echo" class='ativo'"?>>CURSOS</a></li>
			<li><a href="como-ingressar" title="Como Ingressar" id="mn-como-ingressar" <?if($this->router->class=='como_ingressar')echo" class='ativo'"?>>COMO INGRESSAR</a></li>
			<li><a href="processo-seletivo" title="Processo Seletivo" id="mn-processo-seletivo" <?if($this->router->class=='processo-seletivo')echo" class='ativo'"?>>PROCESSO SELETIVO</a></li>
		</ul>
	</nav>

	<div id="menu-wrapper">
		<nav id="menu-vertical">
			<a id="link-portal" href="http://www.sequencialmatriculas.com.br/portalfac/aluno/" title="PORTAL DO ALUNO">PORTAL DO ALUNO</a>
			<ul>
				<li><a href="cursos/graduacao" title="Graduação" id="mn-graduacao" <?if($this->router->method=='graduacao' || isset($ativo) && $ativo=='graduacao')echo" class='ativo'"?>>GRADUAÇÃO</a></li>
				<li><a href="cursos/pos-graduacao" title="Pós-Graduação" id="mn-pos-graduacao" <?if($this->router->method=='pos_graduacao' || isset($ativo) && $ativo=='pos_graduacao')echo" class='ativo'"?>>PÓS-GRADUAÇÃO</a></li>
				<li><a href="cursos/extensao-universitaria" title="Cursos de Extensão" id="mn-extensao-universitaria" <?if($this->router->method=='extensao_universitaria' || isset($ativo) && $ativo=='extensao_universitaria')echo" class='ativo'"?>>CURSOS DE EXTENSÃO</a></li>
				<li><a href="cursos/cursos-livres" title="Cursos Livres" id="mn-cursos-livres" <?if($this->router->method=='cursos_livres' || isset($ativo) && $ativo=='cursos_livres')echo" class='ativo'"?>>CURSOS LIVRES</a></li>
				<li><a href="noticias" title="Notícias" id="mn-noticias" <?if($this->router->class=='noticias')echo" class='ativo'"?>>NOTÍCIAS</a></li>
				<li><a href="eventos" title="Eventos" id="mn-eventos" <?if($this->router->class=='eventos')echo" class='ativo'"?>>EVENTOS</a></li>
				<li><a href="bolsas-de-estudo" title="Bolsas de Estudo" id="mn-bolsas-de-estudo" <?if($this->router->class=='bolsas_de_estudo')echo" class='ativo'"?>>BOLSAS DE ESTUDO</a></li>
				<li><a href="financiamentos" title="Financiamentos" id="mn-financiamentos" <?if($this->router->class=='financiamentos')echo" class='ativo'"?>>FINANCIAMENTOS</a></li>
				<li><a href="estagios" title="Estágios" id="mn-estagios" <?if($this->router->class=='estagios')echo" class='ativo'"?>>ESTÁGIOS</a></li>
				<!-- <li><a href="empresas-parceiras" title="Empresas Parceiras" id="mn-empresas-parceiras" <if($this->router->class=='empresas-parceiras')echo" class='ativo'"?>>EMPRESAS PARCEIRAS</a></li> -->
			</ul>
		</nav>
	</div>

</header>

<div class="main main-<?=$this->router->class?> main-<?=$this->router->class?>-<?=$this->router->method?> centro">