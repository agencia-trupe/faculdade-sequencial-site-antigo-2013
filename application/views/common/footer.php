  
  <?php if ($this->router->class != 'home'): ?>
    </div> <!-- FIM DA DIV MAIN -->  
  <?php endif ?>
  

  <footer>

    <ul>
      <li><a href="home" title="Página Inicial">HOME</a></li>&nbsp;|&nbsp;
      <li><a href="faculdade" title="A Faculdade">A FACULDADE</a></li>&nbsp;|&nbsp;
      <li><a href="como-ingressar" title="Como Ingressar">COMO INGRESSAR</a></li>&nbsp;|&nbsp;
      <li><a href="cursos" title="Cursos">CURSOS</a></li>&nbsp;|&nbsp;
      <li><a href="processo-seletivo" title="Processo Seletivo">PROCESSO SELETIVO</a></li>&nbsp;|&nbsp;
      <li><a href="noticias" title="Notícias">NOTÍCIAS</a></li>&nbsp;|&nbsp;
      <li><a href="eventos" title="Eventos">EVENTOS</a></li>&nbsp;|&nbsp;
      <li><a href="localizacao" title="Localização">LOCALIZAÇÃO</a></li>&nbsp;|&nbsp;
      <li><a href="contato" title="Contato">CONTATO</a></li>&nbsp;|&nbsp;
      <li><a href="ouvidoria" title="Ouvidoria">OUVIDORIA</a></li>&nbsp;|&nbsp;
      <li><a href="http://www.sequencialmatriculas.com.br/faculdade/" target="_blank" title="Portal do Aluno">PORTAL DO ALUNO</a></li>
    </ul>

    <div class="assinatura">
      &copy; <?=date('Y')?> FACULDADE SEQUENCIAL &bull; Todos os direitos reservados &bull; <a href="http://www.trupe.net" target="_blank" title="Criação de Sites">Criação de Sites</a> : <a href="http://www.trupe.net" target="_blank" title="Trupe Agência Criativa">Trupe Agência Criativa</a>
    </div>
  
  </footer>
  
  
  <?if(ENVIRONMENT != 'development' && GOOGLE_ANALYTICS != FALSE):?>
    <script>
      window._gaq = [['_setAccount','UA<?=GOOGLE_ANALYTICS?>'],['_trackPageview'],['_trackPageLoadTime']];
      Modernizr.load({
        load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
      });
    </script>
  <?endif;?>

  <?JS(array('cycle', 'fancybox', 'front'))?>

<div id="ghost-load">
  <span id="sp1"></span>
  <span id="sp2"></span>
  <span id="sp3"></span>
  <span id="sp4"></span>
  <span id="sp5"></span>
  <span id="sp6"></span>
  <span id="sp7"></span>
</div>

</body>
</html>
