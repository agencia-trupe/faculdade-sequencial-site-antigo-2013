<section>
	
	<div id="breadcrumbs">
		<a href="home" title="Página Inicial">home</a>&nbsp;&raquo;&nbsp;
		<a href="localizacao" title="Localização">localização</a>
	</div>
	
	<h1>Localização</h1>

	<iframe class="maps" width="490" height="411" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com.br/maps?f=q&amp;source=s_q&amp;hl=pt-BR&amp;geocode=&amp;q=Rua+Eng%C2%BA+Aluisio+Marques,+S%2FN+&amp;aq=&amp;sll=-23.659295,-46.764969&amp;sspn=0.006732,0.011362&amp;vpsrc=0&amp;ie=UTF8&amp;hq=&amp;hnear=R.+Eng.+Alu%C3%ADsio+Marques+-+Cap%C3%A3o+Redondo,+S%C3%A3o+Paulo,+05854-110&amp;ll=-23.659295,-46.764969&amp;spn=0.006732,0.011362&amp;t=m&amp;z=14&amp;output=embed"></iframe>
	
	<p class="endereco">
		Rua Engº Aluisio Marques, S/N<br>
		Parque Maria Helena<br>
		Campo Limpo / Capão Redondo<br>
		05854-110 - São Paulo - SP
	</p>

	<p>
		Em frente à estação do metrô Capão Redondo | Altura do 3700 da Estrada de Itapecerica
	</p>

	<p class="atendimento">
		CENTRAL DE ATENDIMENTO:<br>
		<span class="telefone">11 3371-2828</span>
	</p>

</section>