<section>

	<div id="breadcrumbs">
		<a href="home" title="Página Inicial">home</a>&nbsp;&raquo;&nbsp;
		<a href="bolsas_de_estudo" title="Bolsas de Estudo">bolsas de estudo</a>
	</div>
	
	<h1>Bolsas de Estudo</h1>

	<h2>PROGRAMAS DE BOLSAS DE ESTUDO DO GOVERNO BRASILEIRO</h2>

	<a href="" title="Bolsas ProUni" data-open="detalhe-prouni" class="link-box">
		<img src="_imgs/layout/bolsas-prouni1-fundobranco.jpg" alt="Bolsas ProUni">
		PROUNI - Programa Universidade<br> para Todos
		<div class="vermais">
			clique para saber mais &raquo;
		</div>
	</a>

	<a href="" title="Bolsas Escola da Família" data-open="detalhe-escola" class="link-box">
		<img src="_imgs/layout/bolsas-familia1-fundobranco.jpg" alt="Bolsas Escola da Família">
		PROGRAMA ESCOLA DA FAMÍLIA - Bolsa Universidade
		<div class="vermais">
			clique para saber mais &raquo;
		</div>
	</a>

	<div class="detalhe" id="detalhe-prouni">
		
		<img src="_imgs/layout/bolsas-prouni2.jpg" alt="ProUni">

		<h3>PROUNI - Programa Universidade para Todos</h3>

		<h4>O que é?</h4>
		<p class="short">
			Prouni - Programa Universidade para Todos tem como finalidade a concessão de bolsas de estudo integrais e parciais em cursos de graduação e sequenciais de formação específica, em instituições privadas de educação superior
		</p>

		<h4>Pra quem é?</h4>		
		<p>
			Dirigido aos estudantes egressos do ensino médio da rede pública ou da rede particular na condição de bolsistas integrais, com renda per capita familiar máxima de três salários mínimos
		</p>

		<h4>Como funciona?</h4>
		<p>
			O Prouni conta com um sistema de seleção informatizado e impessoal, que confere transparência e segurança ao processo. Os candidatos são selecionados pelas notas obtidas no Enem - Exame Nacional do Ensino Médio conjugando-se, desse modo, inclusão à qualidade e mérito dos estudantes com melhores desempenhos acadêmicos.
		</p>
		<p>
			O Prouni possui também ações conjuntas de incentivo à permanência dos estudantes nas instituições, como a Bolsa Permanência, os convênios de estágio MEC/CAIXA e MEC/FEBRABAN e ainda o Fies - Fundo de Financiamento Estudantil, que possibilita ao bolsista parcial financiar até 100% da mensalidade não coberta pela bolsa do programa.
		</p>

		<h4>Como se inscrever?</h4>
		<p>
			Acesse o site : <a href="http://siteprouni.mec.gov.br/" title="Site do ProUni" target="_blank">http://siteprouni.mec.gov.br/</a>
		</p>

	</div>

	<div class="detalhe" id="detalhe-escola">
		
		<img src="_imgs/layout/bolsas-familia2.jpg" alt="Bolsa Universidade">

		<h3>PROGRAMA ESCOLA DA FAMÍLIA – Bolsa Universidade</h3>

		<h4>O que é?</h4>
		<p class="medium">
			O Programa Bolsa-Universidade é um convênio que foi estabelecido entre o Governo do Estado de São Paulo e as Instituições de Ensino Superior, por meio da Secretaria de Estado da Educação para oferecer bolsas de estudo a estudantes do 3º Grau.
		</p>

		<h4>Pra quem é?</h4>		
		<p>
			A bolsa beneficia estudantes de graduação que, em contrapartida, desenvolvem, em escolas estaduais ou municipais (dos municípios conveniados) do Estado de São Paulo,  atividades compatíveis com a natureza de seu curso de graduação e/ou de acordo com as suas habilidades pessoais, aos finais de semana.
		</p>

		<h4>Como funciona?</h4>
		<p>
			O Programa custeia 50% do valor do curso (até o limite estabelecido pelo Programa) e a instituição de ensino se responsabiliza pelo restante. O aluno realiza suas atividades de contrapartida em escolas conveniadas do Estado de São Paulo e não paga nada pelo seu curso de graduação.
		</p>
		
		<h4>Como se inscrever?</h4>
		<p>
			Acesse o site : <a href="http://escoladafamilia.fde.sp.gov.br/" title="Site do Programa Escola da Família" target="_blank">http://escoladafamilia.fde.sp.gov.br/</a>
		</p>

	</div>	

</section>

<script defer>
	$('document').ready( function(){

		var hash = window.location.hash;

		if(hash.indexOf('#') != -1)
			hash = hash.substring(1);

		if(hash)
			$('#detalhe-'+hash).slideDown('normal');


		$('.link-box').click( function(e){
			e.preventDefault();

			var open = $(this).attr('data-open');
			var alvo = $('.detalhe#'+open);
			var visivel = $('.detalhe:visible');

			if(visivel.length){
				visivel.fadeOut('normal', function(){
					alvo.fadeIn('normal');
				});
			}else{
				alvo.fadeIn('normal');
			}
		});

	});
</script>