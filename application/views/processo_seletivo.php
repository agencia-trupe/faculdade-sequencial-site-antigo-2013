<section>

	<div id="breadcrumbs">
		<a href="home" title="Página Inicial">home</a>&nbsp;&raquo;&nbsp;
		<a href="processo-seletivo" title="Processo Seletivo">processo seletivo</a>
	</div>

	<h1>Processo Seletivo</h1>

	<a href="" title="Inscreva-se!" id="banner-topo">
		<img src="_imgs/layout/processoSeletivo-cabec-vestibularSocial2014.png" alt="Processo Seletivo">
	</a>

	<div class="container processo">

		<div class="coluna prim">

			<h2>PARA PARTICIPAR DO PROCESSO SELETIVO - VESTIBULAR SOCIAL 2014 - DA FACULDADE SEQUENCIAL:</h2>

			<ul>
				<li class="um">
					Inscreva-se online para a <strong>Prova de Bolsas de Estudo</strong> preenchendo o cadastro em nosso <a href="http://sequencialmatriculas.com.br/faculdade/" title="Sistema de Inscrição na Prova de Bolsas">Sistema Online</a>.
				</li>
				<li class="dois">
					Agende-se para realizar a prova dia 30 de novembro de 2013 às 10h00 nas dependências da própria Faculdade Sequencial.<br>
					<strong>VOCÊ PODE OBTER DESCONTOS DE ATÉ 100%*!</strong>
					<span class="menor">* Desconto relativo à nota obtida na Prova de Bolsas de Estudo do Vestibular Social 2014 da Faculdade Sequencial.</span>
				</li>
				<li class="tres">
					Confira o resultado da prova, o desconto obtido e matricule-se!
				</li>
				<li class="quatro">
					 Alie mais um benefício para cursar sua faculdade: Se não alcançar 100% de desconto com a sua nota da Prova financie o restante da mensalidade com o FIES. A Faculdade Sequencial é uma faculdade credenciada!
				</li>
			</ul>

		</div>

		<div class="coluna">
			<div class="orange-box">
				Leia o Edital, o Manual do Candidato e o Regulamento da Prova de Bolsas do Vestibular Social 2014:
				<a href="download/_pdfs/edital-faculdadeSequencial-VestibularSocial2014-1" target="_blank" title="Download do EDITAL DO VESTIBULAR SOCIAL 2014">Download do <strong>EDITAL DO VESTIBULAR SOCIAL 2014</strong></a>
				<a href="download/_pdfs/Regulamento-VestibularSocial-2014-01-FaculdadeSequencial" target="_blank" title="Download do REGULAMENTO DA PROVA DE BOLSAS - VESTIBULAR SOCIAL 2014">Download do <strong>REGULAMENTO DA PROVA DE BOLSAS - VESTIBULAR SOCIAL 2014</strong></a>
				<a href="download/_pdfs/manualCandidato-faculdadeSequencial-VestibularSocial2014" target="_blank" title="Download do MANUAL DO CANDIDATO">Download do <strong>MANUAL DO CANDIDATO</strong></a>
			</div>
		</div>
	</div>

	<div class="container list">

		<h2>VENHA FAZER SUA GRADUAÇÃO NA FACULDADE SEQUENCIAL!</h2>

		<ul>
			<li>» Em frente à estação do Metrô Capão Redondo</li>
			<li>» Os melhores laboratórios totalmente equipados</li>
			<li>» Mais de 10 anos de experiência na educação profissionalizante</li>
			<li>» 80% de Professores Mestres e Doutores</li>
			<li>» Salas amplas e confortáveis</li>
			<li>» Aulas práticas</li>
			<li>» Biblioteca especializada</li>
			<li>» Brinquedoteca pedagógica</li>
			<li>» Cantina e área de conveniência</li>
			<li>» Internet Banda Larga e Wi-fi</li>
			<li>» Estágio nos melhores Hospitais da Região (para o curso de enfermagem)</li>
		</ul>

	</div>

</section>