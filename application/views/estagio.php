<section>

	<div id="breadcrumbs">
		<a href="home" title="Página Inicial">home</a>&nbsp;&raquo;&nbsp;
		<a href="estagios" title="Estágios">estágios</a>
	</div>

	<h1>Estágios</h1>

	<p>
		A Faculdade Sequencial possui parcerias de estágios remunerados com instituições especializadas no ingresso de jovens no mercado de trabalho, como o Nube, Ciee e a Fundap. Isso mostra que além de estarmos engajados na missão de formar cidadãos conscientes e ativos, ajudamos e apoiamos a sua colocação no mercado de trabalho.
	</p>

	<div class="detalhe-estagio">
		<div class="imagem">
			<a href="https://www.nube.com.br/" title="Núcleo Brasileiro de Estágios" target="_blank">
				<img src="_imgs/estagios/marca-nube.jpg" alt="Núcleo Brasileiro de Estágios">
			</a>
		</div>

		<p>
			O Nube é uma organização privada de colocação de jovens no mercado de trabalho. Começou atuando como agente de integração entre estudantes, empresas e instituições de ensino. A partir daí, passou a oferecer vagas para aprendizagem, por meio de instituições parceiras, e a Sequencial é uma dessas instituições parceiras do Nube.
		</p>

		<p>
			Os alunos da Sequencial irão encontrar sempre milhares de ofertas para fazer estágio e aprendizagem. As vagas são preenchidas em tempo real e você deve se cadastrar no portal do Nube para localizar a oportunidade mais voltada ao seu perfil e sua carreira.
		</p>

		<p>
			O melhor: todos os serviços oferecidos pelo Nube são de graça!
		</p>

		<p>
			O Nube ainda oferece dicas de profissionais da área de RH para você enfrentar com sucesso uma dinâmica e ainda saber as exigências do mercado para os profissionais do futuro.
		</p>

		<p>
			Fique atento às notícias no portal do Nube, à legislação de estágios e aprendizagem, pesquise suas vagas e boa sorte!
		</p>
	</div>

	<div class="detalhe-estagio">
		<div class="imagem">
			<a href="http://www.ciee.org.br" title="Centro de Integração Empresa-Escola" target="_blank">
				<img src="_imgs/estagios/marca-ciee.jpg" alt="Centro de Integração Empresa-Escola">
			</a>
		</div>
		<p>
			O Centro de Integração Empresa-Escola - CIEE é uma associação filantrópica de direito privado, sem fins lucrativos, beneficente de assistência social e reconhecida de utilidade pública que, dentre vários programas, possibilita aos jovens estudantes brasileiros, uma formação integral, ingressando-os ao mercado de trabalho, através de treinamentos e programas de estágio.
		</p>
		<p>
			O maior objetivo do CIEE, é encontrar, para os estudantes de nível médio, técnico e superior oportunidades de estágio ou aprendizado, que os auxiliem a colocar em prática tudo o que aprenderam na teoria.
		</p>
		<p>
			O cadastramento deve ser realizado conforme perfil do internauta: visitante, estagiário, estudantes com interesse em programas de estágio, adolescente aprendiz, trainees, cursos, oficinas de capacitação, palestas, entre outros.
		</p>
		<p>
			Faça já seu cadastro! Não perca essa oportunidade de ingressar no mercado de trabalho.
		</p>
	</div>

	<div class="detalhe-estagio">
		<div class="imagem">
			<a href="http://novo.fundap.sp.gov.br" title="Fundação do Desenvolvimento Administrativo (Fundap)" target="_blank">
				<img src="_imgs/estagios/marca-fundap.jpg" alt="Fundação do Desenvolvimento Administrativo (Fundap)">
			</a>
		</div>
		<p>
			A Fundap tem por missão contribuir para a elevação dos níveis de eficácia e eficiência da administração pública estadual, desenvolvendo projetos de consultoria organizacional, formação de recursos humanos, desenvolvimento de novas tecnologias de gestão administrativa e pesquisa aplicada.
		</p>

		<p>
			A Fundação do Desenvolvimento Administrativo (Fundap) promove Processo Seletivo para Estudantes que estejam matriculados e frequentando cursos de ensino superior, ensino médio e educação profissional de nível médio para realização de estágios remunerados, em órgãos e entidades da Administração Pública Estadual e Assembleia Legislativa do Estado de São Paulo para as áreas dos cursos.
		</p>

		<p>
			O estágio visa proporcionar ao estudante regularmente matriculado em instituição de ensino pública ou privada – e somente enquanto estiver cursando – o exercício de atividades correlatas à sua formação profissional, em complementação aos conhecimentos teóricos recebidos.
		</p>

		<p>
			A Fundap divulga editais e abre processos seletivos periodicamente, não deixe de acessar o site e ficar sempre atento as informações publicadas.
		</p>
	</div>

</section>