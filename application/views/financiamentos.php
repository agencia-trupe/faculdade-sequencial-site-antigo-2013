<section>

	<div id="breadcrumbs">
		<a href="home" title="Página Inicial">home</a>&nbsp;&raquo;&nbsp;
		<a href="financiamentos" title="Financiamentos">financiamentos</a>
	</div>

	<h1>Financiamentos</h1>

	<h2>PROGRAMAS DE FINANCIAMENTO DO GOVERNO BRASILEIRO</h2>

	<a href="" title="FIES" data-open="detalhe-fies" class="link-box">
		<img src="_imgs/layout/bolsas-fies1-fundobranco.jpg" alt="Fundo de Financiamento Estudantil">
			FIES - Fundo de Financiamento<br>Estudantil
		<div class="vermais">
			clique para saber mais &raquo;
		</div>
	</a>

	<div class="detalhe" id="detalhe-fies">

		<img src="_imgs/layout/bolsas-fies2.jpg" alt="ProUni">

		<h3>FIES – Fundo de Financiamento Estudantil</h3>

		<h4>O que é?</h4>
		<p class="short">
			O Fundo de Financiamento Estudantil (Fies) é um programa do Ministério da Educação destinado a financiar a graduação na educação superior de estudantes matriculados em instituições não gratuitas. Podem recorrer ao financiamento os estudantes matriculados em cursos superiores que tenham avaliação positiva nos processos conduzidos pelo Ministério da Educação.
		</p>
		<p>
			Em 2010 o FIES passou a funcionar em um novo formato. O Fundo Nacional de Desenvolvimento da Educação (FNDE) passou a ser o Agente Operador do Programa e os juros caíram para 3,4% ao ano. Além disso, passou a ser permitido ao estudante solicitar o financiamento em qualquer período do ano.
		</p>
		<p>
			Confira o passo a passo para solicitar o financiamento:
		</p>

		<h4>1º Passo: Inscrição no SisFIES</h4>
		<p>
			O primeiro passo para efetuar a inscrição é acessar o SisFIES e informar os dados solicitados. No primeiro acesso, o estudante informará seu número de Cadastro de Pessoa Física (CPF), sua data de nascimento, um endereço de e-mail válido e cadastrará uma senha que será utilizada sempre que o estudante acessar o sistema. Após informar os dados solicitados, o estudante receberá uma mensagem no endereço de e-mail informado para validação do seu cadastro. A partir daí, o estudante acessará o SisFIES e fará sua inscrição informando seus dados pessoais, do seu curso e instituição e as informações sobre o financiamento solicitado.
		</p>

		<h4>2º Passo: Validação das informações</h4>
		<p>
			Após concluir sua inscrição no SisFIES, o estudante deverá validar suas informações na Comissão Permanente de Supervisão e Acompanhamento (CPSA), em sua instituição de ensino, em até 10 (dez) dias, contados a partir do dia imediatamente posterior ao da conclusão da sua inscrição. A Comissão Permanente de Supervisão e Acompanhamento (CPSA) é o órgão responsável, na instituição de ensino, pela validação das informações prestadas pelo candidato no ato da inscrição.
		</p>

		<h4>3º Passo: Contratação do financiamento</h4>
		<p>
			Após a validação das informações o estudante deverá comparecer a um agente financeiro do FIES em até 10 (dez) dias, contados a partir do terceiro dia útil imediatamente subsequente à data da validação da inscrição pela CPSA, para formalizar a contratação do financiamento.
		</p>
		<p>
			No ato da inscrição no SisFIES, o estudante escolherá a instituição bancária, assim como a agência de sua preferência. Sendo a Caixa Econômica Federal e o Banco do Brasil os atuais Agentes Financeiros do Programa.
		</p>
		<p>
			Atenção! Os prazos para validação da documentação na CPSA e para comparecimento na instituição bancária começam a contar a partir da conclusão da inscrição no SisFIES e da validação da inscrição na CPSA, respectivamente, e não serão interrompidos nos finais de semana ou feriados.
		</p>

		<h4>Como se inscrever?</h4>
		<p>
			Simule o financiamento do seu curso no site do FIES:
		</p>
		<p>
			Pedagogia: <a href="http://sisfiesportal.mec.gov.br/simulacao.php?no_curso=Pedagogia&semestres=7" title="Simulação de Financiamento do Curso de Pedagogia pelo FIES">http://sisfiesportal.mec.gov.br/simulacao.php?no_curso=Pedagogia&semestres=7</a>
		</p>
		<p>
			Enfermagem: <a href="http://sisfiesportal.mec.gov.br/simulacao.php?no_curso=Enfermagem&semestres=10" title="Simulação de Financiamento do Curso de Pedagogia pelo FIES">http://sisfiesportal.mec.gov.br/simulacao.php?no_curso=Enfermagem&semestres=10</a>
		</p>

		<h4>CANAL DE ATENDIMENTO EXCLUSIVO PARA O FIES</h4>
		<p>
			Entre em contato através da Central de Atendimento (11) 3371 2828 e agende um horário com a Consultora Fies. Os atendimentos são individuais, em um ambiente privativo, onde você poderá:
		</p>
		<ul>
			<li>&bull; Esclarecer todas as suas dúvidas;</li>
			<li>&bull; Fazer uma simulação de financiamento;</li>
			<li>&bull; Saber toda a documentação necessária;</li>
			<li>&bull; Entender a forma de pagamento, o prazo de carência e amortização.</li>
		</ul>

	</div>

</section>

<script defer>
	$('document').ready( function(){

		var hash = window.location.hash;

		if(hash.indexOf('#') != -1)
			hash = hash.substring(1);

		if(hash)
			$('#detalhe-'+hash).slideDown('normal');


		$('.link-box').click( function(e){
			e.preventDefault();

			var open = $(this).attr('data-open');
			var alvo = $('.detalhe#'+open);
			var visivel = $('.detalhe:visible');

			if(visivel.length){
				visivel.fadeOut('normal', function(){
					alvo.fadeIn('normal');
				});
			}else{
				alvo.fadeIn('normal');
			}
		});

	});
</script>