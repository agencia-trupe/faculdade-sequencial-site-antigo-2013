<section>

	<div id="breadcrumbs">
		<a href="home" title="Página Inicial">home</a>&nbsp;&raquo;&nbsp;
		<a href="como_ingressar" title="Como Ingressar">como ingressar</a>
	</div>

	<h1>Como Ingressar</h1>

	<div class="topo">
		<h2>Como iniciar minha faculdade?</h2>
		<h3>Existem diversas maneiras de iniciar seu<br>CURSO DE GRADUAÇÃO na FACULDADE SEQUENCIAL.</h3>
		<h4>Conheça todas as opções que selecionamos para você:</h4>
	</div>

	<a class="link-accordion laranja" href="" title="PROCESSO SELETIVO DA FACULDADE SEQUENCIAL - VESTIBULAR TRADICIONAL">PROCESSO SELETIVO DA FACULDADE SEQUENCIAL - VESTIBULAR TRADICIONAL</a>
	<div class="texto paragrafospeq" id="txt-vestibular">
		<a class="float-link" href="processo-seletivo" title="Saiba mais sobre o Processo Seletivo"><img src="_imgs/layout/clique-processo-seletivo.jpg" alt="Saiba mais sobre o Processo Seletivo"></a>
		<p>
			Regularmente antes do início de cada semestre a FACULDADE SEQUENCIAL abre seu <a href="processo-seletivo" title="Processo Seletivo">Processo Seletivo</a> com o Vestibular para preencher as vagas dos <a href="cursos/index/graduacao" title="Cursos de Graduação">Cursos de Graduação</a>.
		</p>

		<p>
			Você acompanha aqui no site as datas de inscrição, provas e as formas de seleção.
		</p>
	</div>

	<a class="link-accordion laranja" href="" title="CADASTRO DE INTERESSE DE VAGAS - PROMOÇÃO E DESCONTOS">CADASTRO DE INTERESSE DE VAGAS - PROMOÇÃO E DESCONTOS</a>
	<div class="texto paragrafospeq" id="txt-vagas">
		<a class="float-link" href="processo-seletivo" title="Saiba mais sobre o Processo Seletivo"><img src="_imgs/layout/clique-processo-seletivo.jpg" alt="Saiba mais sobre o Processo Seletivo"></a>
		<p>
			Você pode obter descontos incríveis direto com a FACULDADE SEQUENCIAL: faça seu <a href="http://www.sequencialmatriculas.com.br/faculdade/" target="_blank" title="Cadastro de Interesse de Vagas">Cadastro de Interesse de Vagas</a> no site e aguarde um contato da equipe de atendimento da Sequencial que lhe fará uma proposta de desconto imperdível para participar do <a href="processo-seletivo" title="Processo Seletivo">Processo Seletivo</a>.
		</p>

		<p>
			Para acompanhar novidades e receber sempre notícias atualizadas sobre o <a href="processo-seletivo" title="Processo Seletivo">Processo Seletivo</a> da FACULDADE SEQUENCIAL mantenha seu <a href="http://www.sequencialmatriculas.com.br/faculdade/" target="_blank" title="Seu Cadastro">cadastro</a> completo e atualizado.
		</p>

	</div>

	<a class="link-accordion laranja" href="" title="ANÁLISE DA NOTA DO ENEM">ANÁLISE DA NOTA DO ENEM</a>
	<div class="texto paragrafospeq" id="txt-enem">
		<a class="float-link" href="processo-seletivo" title="Saiba mais sobre o Processo Seletivo"><img src="_imgs/layout/clique-processo-seletivo.jpg" alt="Saiba mais sobre o Processo Seletivo"></a>
		<p>
			Você pode ser dispensado de participar da prova do  <a href="processo-seletivo" title="Vestibular">Vestibular</a> se optar por fazer diretamente sua matrícula no <a href="cursos/index/graduacao" title="Curso de Graduação">Curso de Graduação</a> de sua preferência usando como método de admissão a Análise da sua Nota do ENEM.
		</p>

		<p>
			Para isso inscreva-se no <a href="processo-seletivo" title="Processo Seletivo">Processo Seletivo</a> normalmente e entre em contato com a Central de Atendimento Sequencial. Saiba mais na página do <a href="processo-seletivo" title="Processo Seletivo">Processo Seletivo</a>.
		</p>
	</div>

	<a class="link-accordion aclaro" href="" title="TRANSFERÊNCIA DE FACULDADE">TRANSFERÊNCIA DE FACULDADE</a>
	<div class="texto" id="txt-transferencia">
		<p>
			Você pode transferir-se de outra faculdade para a FACULDADE SEQUENCIAL. Se você iniciou o curso de <a href="cursos/graduacao/graduacao_em_enfermagem" title="Enfermagem">Enfermagem</a> ou <a href="cursos/graduacao/graduacao_em_pedagogia" title="Pedagogia">Pedagogia</a> em outra instituição e quer vir estudar conosco, acompanhe os passos abaixo:
		</p>

		<p>
			Documentos necessários:<br>
			- Histórico escolar com as disciplinas já cursadas (com a carga horária e notas)<br>
			- Programa das disciplinas<br>
		</p>

		<p>
			A FACULDADE SEQUENCIAL aceitará matrícula por transferência:<br>
			- para cursos afins (Pedagogia e Enfermagem)<br>
			- na existência de vagas<br>
			- após análise da documentação pela Coordenadoria de Curso
		</p>

		<p>
			Entre em contato diretamente com a CENTRAL DE ATENDIMENTO SEQUENCIAL no telefone: 11 3371.2828.
		</p>

	</div>

	<a class="link-accordion aescuro" href="" title="PROGRAMAS DE BOLSAS DE ESTUDO DO GOVERNO BRASILEIRO">PROGRAMAS DE BOLSAS DE ESTUDO DO GOVERNO BRASILEIRO</a>
	<div class="texto" style="height:227px;" id="txt-bolsas">
		<p>
			Você pode inscrever-se em um dos Programas de Bolsas de Estudo do Governo Brasileiro para realizar seu curso na FACULDADE SEQUENCIAL com gratuidade concedida pelo Governo do País, Estado ou Município.
		</p>
		<p>
			A FACULDADE SEQUENCIAL é inscrita e aceita alunos que participam dos seguintes Programas:
		</p>
		<a href="bolsas-de-estudo/#prouni" title="Bolsas ProUni" class="link-box">
			<img src="_imgs/layout/bolsas-prouni1-fundobranco.jpg" alt="Bolsas ProUni">
			PROUNI - Programa Universidade<br> para Todos
			<div class="vermais">
				clique para saber mais &raquo;
			</div>
		</a>
		<a href="bolsas-de-estudo/#escola" title="Bolsas Escola da Família" class="link-box">
			<img src="_imgs/layout/bolsas-familia1-fundobranco.jpg" alt="Bolsas Escola da Família">
			PROGRAMA ESCOLA DA FAMÍLIA - Bolsa Universidade
			<div class="vermais">
				clique para saber mais &raquo;
			</div>
		</a>
	</div>

	<a class="link-accordion aescuro" href="" title="PROGRAMAS DE FINANCIAMENTO DO GOVERNO BRASILEIRO">PROGRAMAS DE FINANCIAMENTO DO GOVERNO BRASILEIRO</a>
	<div class="texto" id="txt-financiamentos">
		<p>
			O Ministério da Educação oferece um Financiamento específico para universitários: o FIES. Com ele o estudante paga uma taxa mínima durante o curso e começa a pagar as parcelas do financiamento após o término da graduação.
		</p>

		<p>
			Para participar do FIES aluno e instituição devem estar inscritos no Programa e atender a algumas exigências. Saiba mais:
		</p>

		<a href="financiamentos/#fies" title="Fundo de Financiamento Estudantil" class="link-box">
			<img src="_imgs/layout/bolsas-fies1-fundobranco.jpg" alt="Fundo de Financiamento Estudantil">
			FIES - Fundo de Financiamento<br>Estudantil
			<div class="vermais">
				clique para saber mais &raquo;
			</div>
		</a>
	</div>

</section>

<script defer>
	$('document').ready( function(){

		var hash = window.location.hash;

		if(hash.indexOf('#') != -1)
			hash = hash.substring(1);

		if(hash)
			$('#txt-'+hash).slideDown('normal');
		

		$('.link-accordion').click( function(e){
			e.preventDefault();

			var botao = $(this);
			var texto = $(this).next('.texto');

			$('.link-accordion.ativo').removeClass('ativo');

			if(texto.is(':hidden')){

				if ($('.texto:visible').length > 0) {

					$('.texto:visible').slideUp('normal', function(){
						texto.slideDown('normal');
					});

				}else{
					texto.slideDown('normal');
				}

				botao.addClass('ativo');

			}else{
				texto.slideUp('normal');
			}

		})

	});
</script>