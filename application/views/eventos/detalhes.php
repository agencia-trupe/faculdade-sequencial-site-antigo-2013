<section>

	<div id="breadcrumbs">
		<a href="home" title="Página Inicial">home</a>&nbsp;&raquo;&nbsp;
		<a href="eventos/index" title="Eventos">eventos</a>&nbsp;&raquo;&nbsp;
		<a href="eventos/detalhes/<?=$detalhe[0]->slug?>" title="<?=$detalhe[0]->titulo?>"><?=word_limiter(minuscula($detalhe[0]->titulo), 6)?></a>
	</div>

	<a class="link-voltar" href="javascript:window.history.back(-2)" title="Voltar">&laquo; voltar para a home de eventos</a>

	<h1>Eventos</h1>

	<div class="detalhe">

		<?php if (isset($detalhe[0]) && $detalhe[0]): ?>

			<div class="topo">
				<div class="data">
					<span class="dia"><?=dia($detalhe[0]->data)?></span><br>
					<span class="mes"><?=maiuscula(mes($detalhe[0]->data, TRUE))?></span><br>
					<span class="ano"><?=ano($detalhe[0]->data)?>
				</div>
				<div class="titulos">
					<div class="detalhe_data"><?=$detalhe[0]->data_detalhe?></div>
					<h2><?=$detalhe[0]->titulo?></h2>
					<?php if ($detalhe[0]->imagem): ?>
						<div class="imagem">
							<img src="_imgs/eventos/<?=$detalhe[0]->imagem?>">
						</div>
					<?php endif ?>
				</div>
			</div>

			<?php if ($detalhe[0]->olho): ?>
				<div class="olho">
					<?=$detalhe[0]->olho?>
				</div>
			<?php endif ?>

			<div class="texto">
				<?= relativizaUrl($detalhe[0]->texto) ?>
			</div>

			<?php if ($galeria): ?>

				<div class="galeria">

					<h3>Veja mais imagens do evento:</h3>

					<?php foreach ($galeria as $key => $value): ?>
						<a href="_imgs/eventos/<?=$value->imagem?>" rel="galeria" class="fancy"><img src="_imgs/eventos/thumbs/<?=$value->imagem?>" title="" alt=""></a>
					<?php endforeach ?>
				</div>

			<?php endif ?>

		<?php endif; ?>

	</div>

	<a href="javascript:window.history.back(-2)" title="Voltar" class="link-voltar-inferior">&laquo; voltar para a home de eventos</a>

</section>

<script defer></script>