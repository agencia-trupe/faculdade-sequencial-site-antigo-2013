<section>

	<div id="breadcrumbs">
		<a href="home" title="Página Inicial">home</a>&nbsp;&raquo;&nbsp;
		<a href="eventos" title="Eventos">eventos</a>
	</div>

	<h1>Eventos</h1>

	<?php if ($proximos_eventos): ?>

		<h2 class="titulo-laranja">PRÓXIMOS EVENTOS</h2>

		<div class="proximos">

			<?php $contador = 0 ?>

			<?php foreach ($proximos_eventos as $key => $value): ?>
				
				<a class="link-destaques<?php if($contador%2==0)echo " commargem"?>" href="eventos/detalhes/<?=$value->slug?>" title="<?=$value->titulo?>">
					<div class="data"><?=dia($value->data).' '.maiuscula(mes($value->data, TRUE)).' '.ano($value->data)?></div>
					<?php if ($value->imagem): ?>
						<div class="imagem">
							<img src="_imgs/eventos/thumbs/<?=$value->imagem?>" alt="<?=$value->titulo?>">
						</div>
					<?php endif ?>
					<div class="titulo"><?=$value->titulo?></div>
				</a>

				<?$contador++?>

			<?php endforeach ?>

		</div>

	<?php endif ?>


	<?php if ($eventos): ?>

		<h2 class="titulo-laranja">HISTÓRICO DE EVENTOS</h2>

		<div class="historico">

			<?php foreach ($eventos as $key => $value): ?>
				
				<a class="link-eventos" href="eventos/detalhes/<?=$value->slug?>" title="<?=$value->titulo?>">
					<div class="data">
						<?php if ($value->data): ?>
							<div class="dia-e-mes"><?=dia($value->data)?>&nbsp;<span class="mes"><?=mes($value->data, true)?></span></div>
							<div class="ano"><?=ano($value->data)?></div>
						<?php endif ?>
					</div>
					<div class="titulo"><?=$value->titulo?></div>
				</a>

			<?php endforeach ?>

			<?php if ($paginacao): ?>
				<div id="paginacao"><?php echo $paginacao ?></div>
			<?php endif ?>

		</div>

	<?php endif ?>

</section>