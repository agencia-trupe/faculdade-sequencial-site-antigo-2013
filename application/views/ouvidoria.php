<section>

	<div id="breadcrumbs">
		<a href="home" title="Página Inicial">home</a>&nbsp;&raquo;&nbsp;
		<a href="ouvidoria" title="Ouvidoria">ouvidoria</a>
	</div>
	
	<h1>Ouvidoria</h1>

	<div id="form">

		<h2>Ouvidoria Corporativa Sequencial</h2>

		<?php if ($this->session->flashdata('enviado')): ?>

			<div class="mensagem">
				Sua mensagem foi enviada com sucesso!<br>
				Responderemos assim que possível.
			</div>
			
		<?php else: ?>

			<form method="post" action="ouvidoria/enviar">

				<input type="text" name="nome" placeholder="nome" required id="input-nome">

				<input type="text" name="cpf" maxlength="11" placeholder="CPF" required id="input-cpf">

				<input type="email" name="email" placeholder="email" required id="input-email">

				<input type="text" name="telefone" placeholder="telefone" required id="input-telefone">

				<textarea name="mensagem" placeholder="mensagem" required id="input-mensagem"></textarea>

				<input type="submit" value="ENVIAR">

			</form>

		<?php endif ?>
		
	</div>

	<h2>Ouvidoria Corporativa Sequencial</h2>

	<p>
		A <strong>Ouvidoria Corporativa Sequencial</strong> é um canal direto do aluno junto à administração do Grupo Sequencial sem entraves ou burocracias.  É a instância final de recorrência, caso o problema apresentado não tenha sido resolvido de maneira satisfatória nos demais serviços de apoio.
	</p>

	<p>
		<strong>Quem pode recorrer à Ouvidoria?</strong><br>
		Todos os alunos e ex-alunos que se sentirem insatisfeitos com a solução apresentada para suas reclamações e/ou que não concordem com a decisão adotada pela faculdade, desde que já tenham recorrido aos serviços disponíveis na Secretaria, Tesouraria, Diretoria ou Setor de Apoio ao Aluno da instituição e não tenham recorrido à esfera judicial poderão entrar em contato com a Ouvidoria Sequencial.
	</p>

	<p>
		<strong>Como o aluno poderá recorrer à Ouvidoria Sequencial?</strong><br>
		O contato com a Ouvidoria Corporativa Sequencial poderá ser efetuado por meio do formulário ao lado. Todos os campos são obrigatórios.
	</p>
		
	</p>

</section>

<script defer>

function valida_cpf(cpf){
      var numeros, digitos, soma, i, resultado, digitos_iguais;
      digitos_iguais = 1;
      if (cpf.length < 11)
            return false;
      for (i = 0; i < cpf.length - 1; i++)
            if (cpf.charAt(i) != cpf.charAt(i + 1))
                  {
                  digitos_iguais = 0;
                  break;
                  }
      if (!digitos_iguais)
            {
            numeros = cpf.substring(0,9);
            digitos = cpf.substring(9);
            soma = 0;
            for (i = 10; i > 1; i--)
                  soma += numeros.charAt(10 - i) * i;
            resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
            if (resultado != digitos.charAt(0))
                  return false;
            numeros = cpf.substring(0,10);
            soma = 0;
            for (i = 11; i > 1; i--)
                  soma += numeros.charAt(11 - i) * i;
            resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
            if (resultado != digitos.charAt(1))
                  return false;
            return true;
            }
      else
            return false;
}

	$('document').ready( function(){

		$('#input-cpf').keyup( function(){
			$(this).val($(this).val().replace(/\D/g, ''));
		});

		$('form').submit( function(){

			if($('#input-nome').val() == '' || $('#input-nome').val() == 'nome'){
				alert('Informe seu nome.');
				return false;
			}
			if($('#input-email').val() == '' || $('#input-email').val() == 'e-mail'){
				alert('Informe seu e-mail.');
				return false;
			}
			if($('#input-telefone').val() == '' || $('#input-telefone').val() == 'telefone'){
				alert('Informe um telefone para contato.');
				return false;
			}
			if($('#input-mensagem').val() == '' || $('#input-mensagem').val() == 'mensagem'){
				alert('Informe sua mensagem.');
				return false;
			}
			if($('#input-cpf').val() == '' || $('#input-cpf').val() == 'CPF'){
				alert('Informe seu CPF.');
				return false;
			}
			if(!valida_cpf($('#input-cpf').val())){
				alert('Informe um CPF válido.');
				return false;	
			}

		});

	});
</script>