<section>

	<div id="breadcrumbs">
		<a href="home" title="Página Inicial">home</a>&nbsp;&raquo;&nbsp;
		<a href="faculdade" title="A Faculdade">a faculdade</a>
	</div>

	<h1>A Faculdade</h1>

	<div class="topo">
		<div class="coluna margem">
			<p>
				A FACULDADE SEQUENCIAL surgiu da necessidade de dar continuidade ao processo educativo dos alunos do nível técnico e oferecer educação de qualidade à comunidade local.
			</p>
			<p>
				Com uma grande experiência acumulada no ensino técnico e preocupado com a completa formação de seus alunos, o Grupo Sequencial identificou o desejo destes em aprimorar suas formações profissionais conquistando o diploma do ensino superior.
			</p>
			<p>
				Então, é com imenso prazer que o Grupo Sequencial cria a FACULDADE SEQUENCIAL para oferecer a todos a oportunidade de uma formação profissional completa.
			</p>
			<p>
				Credenciada pela Portaria MEC nº 965/2011, de 19/7/2011 (publicada no DOU em 20/7/2011), a Faculdade inicia o ano de 2012 com vagas para os cursos superiores de Pedagogia e Enfermagem. Acompanhe aqui pelo site a divulgação dos <a href="processo-seletivo" title="Processos Seletivos">Processos Seletivos</a> para início do 1º e 2º Semestre de cada ano.
			</p>
		</div>
		<div class="coluna">
			<img class="img-fachada" src="_imgs/layout/img-fachada-faculdade.jpg" alt="Fachada da Faculdade Sequencial">

			<!--
			<a class="link-download" href="download/_pdfs/estatuto" title="Download do Estatuto Oficial">
				<img src="_imgs/layout/marca-sequencial-vertical.png" alt="Faculdade Sequencial">
				<div class="texto-branco">
					<img src="_imgs/layout/frase-MEC.png">
				</div>
				<div class="texto-marinho">
					Credenciada pela<br>
					Portaria MEC nº 965/2011,<br>
					de 19/07/2011<br>
					<span style="font-size:10px;">(publicada no DOU em 20/07/2011)</span>
					<div class="faixa">&raquo; DOWNLOAD DO ESTATUTO OFICIAL</div>
				</div>
			</a>
			-->

			<a href="download/_pdfs/relatorio-autoavaliacao-sequencial" class="link-autoavaliacao" title="Download da Autoavaliação Institucional">
				&raquo; DOWNLOAD DA <span>AUTOAVALIAÇÃO INSTITUCIONAL 2012</span>
			</a>

		</div>
	</div>

	<div class="infra">

		<h2>Nossa Infraestrutura</h2>

		<div class="coluna margem">
			<p>
				A Faculdade Sequencial já nasce com uma grande infraestrutura. O Grupo Sequencial com grande experiência em educação de Escolas Técnicas aproveitou todo seu conhecimento acumulado na área de educação para oferecer a melhor infraestrutura que uma nova Faculdade poderia ter. A Faculdade Sequencial conta com salas de aula equipadas, espaço multimídia, laboratório de informática, e todo o apoio necessário ao aluno como secretaria, tesouraria, coordenação, etc. Conta também com espaços de convívio e pesquisa como o pátio, a quadra poliesportiva e a biblioteca. Além disso oferece itens de comodidade como o estacionamento e o refeitório/cantina.
			</p>
			<p style="font-weight:bold;">
				UNIDADE SEDE - Campo Limpo:<br>
				» Salas de aula<br>
				» Biblioteca<br>
				» Sala multimídia<br>
				» Área de convivência<br>
				» Apoio ao aluno<br>
				» Quadra poliesportiva<br>
				» Lanchonete<br>
				» Estacionamento<br>
				» Sala dos Professores
			</p>
			<p style="font-weight:bold;">
				LABORATÓRIOS:<br>
				» Laboratório de Enfermagem<br>
				» Laboratório de Anatomia<br>
				» Laboratório de Microbiologia<br>
				» Laboratório de Informática<BR>
				» Brinquedoteca: Laboratório de Práticas Pedagógicas
			</p>
		</div>
		<div class="coluna">

			<a class="fancy" rel="galeria" href="_imgs/infra/infra_001.jpg"><img class="thumb" src="_imgs/infra/thumb_infra_001.jpg" alt="Imagem Infraestrutura"></a>

            <a class="fancy" rel="galeria" href="_imgs/infra/infra_002.jpg"><img class="thumb" src="_imgs/infra/thumb_infra_002.jpg" alt="Imagem Infraestrutura"></a>

            <a class="fancy" rel="galeria" href="_imgs/infra/infra_003.jpg"><img class="thumb" src="_imgs/infra/thumb_infra_003.jpg" alt="Imagem Infraestrutura"></a>

            <a class="fancy sem-margem" rel="galeria" href="_imgs/infra/infra_004.jpg"><img class="thumb" src="_imgs/infra/thumb_infra_004.jpg" alt="Imagem Infraestrutura"></a>

            <a class="fancy" rel="galeria" href="_imgs/infra/infra_005.jpg"><img class="thumb" src="_imgs/infra/thumb_infra_005.jpg" alt="Imagem Infraestrutura"></a>

            <a class="fancy" rel="galeria" href="_imgs/infra/infra_006.jpg"><img class="thumb" src="_imgs/infra/thumb_infra_006.jpg" alt="Imagem Infraestrutura"></a>

            <a class="fancy" rel="galeria" href="_imgs/infra/infra_007.jpg"><img class="thumb" src="_imgs/infra/thumb_infra_007.jpg" alt="Imagem Infraestrutura"></a>

            <a class="fancy sem-margem" rel="galeria" href="_imgs/infra/infra_008.jpg"><img class="thumb" src="_imgs/infra/thumb_infra_008.jpg" alt="Imagem Infraestrutura"></a>

            <a class="fancy" rel="galeria" href="_imgs/infra/infra_009.jpg"><img class="thumb" src="_imgs/infra/thumb_infra_009.jpg" alt="Imagem Infraestrutura"></a>

            <a class="fancy" rel="galeria" href="_imgs/infra/infra_010.jpg"><img class="thumb" src="_imgs/infra/thumb_infra_010.jpg" alt="Imagem Infraestrutura"></a>

            <a class="fancy" rel="galeria" href="_imgs/infra/infra_011.jpg"><img class="thumb" src="_imgs/infra/thumb_infra_011.jpg" alt="Imagem Infraestrutura"></a>

            <a class="fancy sem-margem" rel="galeria" href="_imgs/infra/infra_012.jpg"><img class="thumb" src="_imgs/infra/thumb_infra_012.jpg" alt="Imagem Infraestrutura"></a>

            <a class="fancy" rel="galeria" href="_imgs/infra/infra_013.jpg"><img class="thumb" src="_imgs/infra/thumb_infra_013.jpg" alt="Imagem Infraestrutura"></a>

            <a class="fancy" rel="galeria" href="_imgs/infra/infra_014.jpg"><img class="thumb" src="_imgs/infra/thumb_infra_014.jpg" alt="Imagem Infraestrutura"></a>

            <a class="fancy" rel="galeria" href="_imgs/infra/infra_015.jpg"><img class="thumb" src="_imgs/infra/thumb_infra_015.jpg" alt="Imagem Infraestrutura"></a>

            <a class="fancy sem-margem" rel="galeria" href="_imgs/infra/infra_016.jpg"><img class="thumb" src="_imgs/infra/thumb_infra_016.jpg" alt="Imagem Infraestrutura"></a>

            <a class="fancy" rel="galeria" href="_imgs/infra/infra_017.jpg"><img class="thumb" src="_imgs/infra/thumb_infra_017.jpg" alt="Imagem Infraestrutura"></a>

            <a class="fancy" rel="galeria" href="_imgs/infra/infra_018.jpg"><img class="thumb" src="_imgs/infra/thumb_infra_018.jpg" alt="Imagem Infraestrutura"></a>

            <a class="fancy" rel="galeria" href="_imgs/infra/infra_019.jpg"><img class="thumb" src="_imgs/infra/thumb_infra_019.jpg" alt="Imagem Infraestrutura"></a>

            <a class="fancy sem-margem" rel="galeria" href="_imgs/infra/infra_020.jpg"><img class="thumb" src="_imgs/infra/thumb_infra_020.jpg" alt="Imagem Infraestrutura"></a>

            <div class="texto-amarelo">
            	<div class="caf">
            		Venha descobrir como é bom fazer parte de<br> uma instituição estruturada e com muito a oferecer.
            	</div>
            	Venha para a FACULDADE SEQUENCIAL!
            </div>

		</div>

	</div>

</section>