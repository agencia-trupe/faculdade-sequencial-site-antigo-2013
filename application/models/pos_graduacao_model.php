<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pos_graduacao_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'pos_graduacao';
		
		$this->dados = array(
			'titulo',
			'subtitulo',
			'portaria',
			'edital',
			'slug',
			'cor'
		);
		$this->dados_tratados = array(
			'edital' => $this->sobeArquivo(),
			'slug' => url_title($this->input->post('titulo'), '_', TRUE)
		);
	}

	public function sobeArquivo(){
		
		$this->load->library('upload');

		$original = array(
			'dir' => '_pdfs/',
			'campo' => 'userfile'
		);

		$uploadconfig = array(
		  'upload_path' => $original['dir'],
		  'allowed_types' => 'pdf',
		  'max_size' => '0',
		  'max_width' => '0',
		  'max_height' => '0');

		$this->upload->initialize($uploadconfig);

		if(isset($_FILES[$original['campo']]) && $_FILES[$original['campo']]['error'] != 4){
		    if(!$this->upload->do_upload($original['campo'])){
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = url_title($arquivo['file_name'], 'underscore', true);
		        rename($original['dir'].$arquivo['file_name'] , $original['dir'].$filename);
		        
		        return $filename;
		    }
		}else{
		    return false;
		}			
		
	}

}