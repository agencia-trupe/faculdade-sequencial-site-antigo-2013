<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mural_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'mural';
		
		$this->dados = array(
			'texto',
			'destino'
		);
		$this->dados_tratados = array();
	}

	function pegarTodos($order_campo = 'ordem', $order = 'ASC'){
		return $this->db->order_by($order_campo, $order)->get($this->tabela)->result();
	}	

}