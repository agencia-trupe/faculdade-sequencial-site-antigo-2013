<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Slides_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'slides';

		$this->dados = array(
			'titulo',
			'imagem',
			'destino'
		);
		$this->dados_tratados = array(
			'imagem' => $this->sobeImagem()
		);
	}

	function sobeImagem(){
		$this->load->library('upload');

		$original = array(
			'dir' => '_imgs/slides/',
			'x' => '765',
			'y' => '400',
			'corte' => 'resize_crop',
			'campo' => 'userfile'
		);

		$uploadconfig = array(
		  'upload_path' => $original['dir'],
		  'allowed_types' => 'jpg|png|gif',
		  'max_size' => '0',
		  'max_width' => '0',
		  'max_height' => '0');

		$this->upload->initialize($uploadconfig);

		if(isset($_FILES[$original['campo']]) && $_FILES[$original['campo']]['error'] != 4){
		    if(!$this->upload->do_upload($original['campo'])){
		    	echo "<pre>";
		    	print_r($this->upload);
		    	echo "</pre>";
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = url_title($arquivo['file_name'], 'underscore', true);
		        rename($original['dir'].$arquivo['file_name'] , $original['dir'].$filename);

		        $this->image_moo
	                 ->load($original['dir'].$filename)
	                 ->$original['corte']($original['x'], $original['y'])
	                 ->save($original['dir'].$filename, TRUE);

		        return $filename;
		    }
		}else{
		    return false;
		}
	}

}