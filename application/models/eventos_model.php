<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Eventos_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'eventos';
		$this->tabela_imagens = 'eventos_imagens';

		$this->dados = array(
			'titulo',
			'slug',
			'data',
			'data_detalhe',
			'imagem',
			'texto',
			'olho'
		);
		$this->dados_tratados = array(
			'data' => formataData($this->input->post('data'), 'br2mysql'),
			'imagem' => $this->sobeImagem('resize', 345, 150)
		);
	}


	function inserir(){

        $this->dados_tratados['slug'] = url_title($this->input->post("titulo"), '_', TRUE);
        $consulta_slug = $this->db->get_where($this->tabela, array('slug' => $this->dados_tratados['slug']))->num_rows();
        $add = 1;
        $check_slug = $this->dados_tratados['slug'];
        while($consulta_slug != 0){
            $check_slug = $this->dados_tratados['slug'].'_'.$add;
            $consulta_slug = $this->db->get_where($this->tabela, array('slug' => $check_slug))->num_rows();
            $add++;
        }
        $this->dados_tratados['slug'] = $check_slug;

		foreach($this->dados as $k => $v){
			if(array_key_exists($v, $this->dados_tratados))
				$this->db->set($v, $this->dados_tratados[$v]);
			elseif($this->input->post($v) !== FALSE)
				$this->db->set($v, $this->input->post($v));
		}
		return $this->db->insert($this->tabela);
	}

	function pegarPaginado($por_pagina, $inicio, $order_campo = 'id', $order = 'DESC'){
		return $this->db->where('data <', date('Y-m-d'))->order_by($order_campo, $order)->get($this->tabela, $por_pagina, $inicio)->result();
	}

	function pegarTodos($order_campo = 'data', $order = 'DESC'){
		return $this->db->order_by($order_campo, $order)->get($this->tabela)->result();
	}

	function sobeImagem($thumb_corte = 'resize_crop', $thumb_x = 100, $thumb_y = 100){
		$this->load->library('upload');

		$original = array(
			'dir' => '_imgs/eventos/',
			'x' => '615',
			'y' => '410',
			'corte' => 'resize',
			'campo' => 'userfile'
		);

		$uploadconfig = array(
		  'upload_path' => $original['dir'],
		  'allowed_types' => 'jpg|png|gif',
		  'max_size' => '0',
		  'max_width' => '0',
		  'max_height' => '0');

		$this->upload->initialize($uploadconfig);

		if(isset($_FILES[$original['campo']]) && $_FILES[$original['campo']]['error'] != 4){
		    if(!$this->upload->do_upload($original['campo'])){
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = url_title($arquivo['file_name'], 'underscore', true);
		        rename($original['dir'].$arquivo['file_name'] , $original['dir'].$filename);

		        if ($thumb_x == 100) {

			        $this->image_moo
		                ->load($original['dir'].$filename)
		                ->$thumb_corte($thumb_x, $thumb_y)
		                ->save($original['dir'].'thumbs/'.$filename);

		        } else {

			        $this->image_moo
		                ->load($original['dir'].$filename)
		                ->$original['corte']($original['x'], $original['y'])
		                ->save($original['dir'].$filename)
		                ->$thumb_corte($thumb_x, $thumb_y)
		                ->save($original['dir'].'thumbs/'.$filename);

		        }

		        return $filename;
		    }
		}else{
		    return false;
		}
	}

}