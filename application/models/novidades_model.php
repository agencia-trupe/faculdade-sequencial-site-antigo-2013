<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Novidades_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'novidades';
		
		$this->dados = array(
			'texto',
			'destino'
		);
		$this->dados_tratados = array();
	}

}