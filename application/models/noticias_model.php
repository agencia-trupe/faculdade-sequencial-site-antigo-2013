<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Noticias_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'noticias';
		
		$this->dados = array(
			'titulo',
			'slug',
			'data',
			'imagem',
			'texto',
			'olho'
		);
		$this->dados_tratados = array(
			'data' => formataData($this->input->post('data'), 'br2mysql'),
			'imagem' => $this->sobeImagem()
		);
	}


	function inserir(){

        $this->dados_tratados['slug'] = url_title($this->input->post("titulo"), '_', TRUE);
        $consulta_slug = $this->db->get_where($this->tabela, array('slug' => $this->dados_tratados['slug']))->num_rows();
        $add = 1;
        $check_slug = $this->dados_tratados['slug'];
        while($consulta_slug != 0){
            $check_slug = $this->dados_tratados['slug'].'_'.$add;
            $consulta_slug = $this->db->get_where($this->tabela, array('slug' => $check_slug))->num_rows();
            $add++;
        }
        $this->dados_tratados['slug'] = $check_slug;		
		
		foreach($this->dados as $k => $v){
			if(array_key_exists($v, $this->dados_tratados))
				$this->db->set($v, $this->dados_tratados[$v]);
			elseif($this->input->post($v) !== FALSE)
				$this->db->set($v, $this->input->post($v));
		}
		return $this->db->insert($this->tabela);
	}	

	function sobeImagem(){
		$this->load->library('upload');

		$original = array(
			'dir' => '_imgs/noticias/',
			'x' => '715',
			'y' => '999',
			'corte' => 'resize',
			'campo' => 'userfile'
		);

		$uploadconfig = array(
		  'upload_path' => $original['dir'],
		  'allowed_types' => 'jpg|png|gif',
		  'max_size' => '0',
		  'max_width' => '0',
		  'max_height' => '0');

		$this->upload->initialize($uploadconfig);

		if(isset($_FILES[$original['campo']]) && $_FILES[$original['campo']]['error'] != 4){
		    if(!$this->upload->do_upload($original['campo'])){
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = url_title($arquivo['file_name'], 'underscore', true);
		        rename($original['dir'].$arquivo['file_name'] , $original['dir'].$filename);
		        
		        $this->image_moo
	                ->load($original['dir'].$filename)
	                ->$original['corte']($original['x'], $original['y'])
	                ->save($original['dir'].$filename, TRUE)
	                ->set_background_colour('#0072AE')
	                ->resize(150, 100, TRUE)
	                ->save($original['dir'].'thumbs/'.$filename, TRUE);

		        return $filename;
		    }
		}else{
		    return false;
		}		
	}	

}