<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Chamadas_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'chamadas';
		
		$this->dados = array(
			'titulo',
			'destino'
		);
		$this->dados_tratados = array();
	}

}