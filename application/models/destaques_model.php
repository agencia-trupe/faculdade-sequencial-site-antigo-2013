<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Destaques_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'destaques';
		
		$this->dados = array(
			'titulo',
			'texto',
			'destino'
		);
		$this->dados_tratados = array();
	}

}