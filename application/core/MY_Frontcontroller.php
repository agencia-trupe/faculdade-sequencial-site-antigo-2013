<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property CI_Loader $load
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Email $email
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 */
class MY_Frontcontroller extends CI_controller {

    var $headervar,
        $footervar,
        $menuvar,
        $asidevar;

    function __construct($css = '', $js = '') {
        parent::__construct();
        
        $this->headervar['load_css'] = $css;
        $this->headervar['load_js'] = $js;

        $this->asidevar['destaques'] = $this->db->get('destaques')->result();
        //$this->output->enable_profiler(TRUE);
    }
    
    function _output($output){

        $sem_aside = array(
            'home'
        );

        if(in_array($this->router->class, $sem_aside)):

            echo $this->load->view('common/header', $this->headervar, TRUE).
                 $this->load->view('common/menu', $this->menuvar, TRUE).
                 $output.
                 $this->load->view('common/footer', $this->footervar, TRUE);

        else:

            echo $this->load->view('common/header', $this->headervar, TRUE).
                 $this->load->view('common/menu', $this->menuvar, TRUE).
                 $output.
                 $this->load->view('common/aside-destaques', $this->asidevar, TRUE).
                 $this->load->view('common/footer', $this->footervar, TRUE);

        endif;
    }

}
?>